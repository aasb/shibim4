use std::collections::HashMap;
use std::fmt::Display;
use crate::base::*;
use crate::util::Itertools;
use unicode_normalization::UnicodeNormalization;

#[derive(Clone, Copy)]
pub enum Lang{
    EN,
    ES
}
lazy_static::lazy_static! {
    pub static ref EN: HashMap<&'static str, &'static str> = {
        HashMap::from([
            ("Mon", "Mon"),
            ("Tue", "Tue"),
            ("Wed", "Wed"),
            ("Thu", "Thu"),
            ("Fri","Fri"),
            ("Sat","Sat"),
            ("Sun","Sat")
        ])
    };
    pub static ref CHORD_KEYWORDS_NAMES: HashMap<ChordKeyword, &'static str> = {
        HashMap::from([
            (ChordKeyword::Sus2,"sus2"),
            (ChordKeyword::Sus4,"sus4"),
            (ChordKeyword::Add2,"add2"),
            (ChordKeyword::Add4,"add4"),
            (ChordKeyword::Add9,"add9"),
            (ChordKeyword::Add11,"add11"),
            (ChordKeyword::Dim,"dim"),
            (ChordKeyword::Aug,"aug"),
            (ChordKeyword::Maj,"Δ"),
            (ChordKeyword::K5,"5"),
            (ChordKeyword::K6,"6"),
            (ChordKeyword::K7,"7"),
            (ChordKeyword::K9,"9"),
            (ChordKeyword::K11,"11"),
            (ChordKeyword::K13,"13"),
            (ChordKeyword::K69,"6/9"),
            (ChordKeyword::Sus,"sus")
        ])
    };
}

//Maybe return iterator
pub fn normalize_text(to_norm : &str, lang : Lang) -> String{
    match lang{
        Lang::EN =>
            to_norm
            .chars()
            .filter_map( |x|{
                if x.is_alphabetic() || x == '\''{
                    Some(x)
                }else if x.is_whitespace(){
                    Some(' ')
                }else{
                    None
                }
            })
            .dedup_by(|a,b|*a==*b && *a ==' ')
            .flat_map(|c|c.to_lowercase())
            .collect::<String>()
        ,
        Lang::ES=> 
            to_norm
            .nfd()
            .filter_map( |x|{
                if x.is_alphabetic() || x == '\u{0303}' {
                    Some(x)
                }else if x.is_whitespace(){
                    Some(' ')
                }else{
                    None
                }
            })
            .dedup_by(|a,b|*a==*b && *a ==' ')
            .nfc()
            .flat_map(|c|c.to_lowercase())
            .collect::<String>()
    }
}


pub struct SongNormTextWrite<'i>(&'i Song,Lang);
pub struct LineNormTextWrite<'i>(&'i Line,Lang);


impl Song{
    pub fn norm_text(&self,l : Lang) -> SongNormTextWrite{
        SongNormTextWrite(self, l)
    }
}
impl Line{
    pub fn norm_text(&self, l : Lang) -> LineNormTextWrite{
        LineNormTextWrite(self, l)
    }
}



impl<'i> Display for SongNormTextWrite<'i>{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for sblock in &self.0.sections{
            if let SongBlock::Section(section) = sblock{
                for subs in &section.subsections{
                    for line in &subs.lines{
                        write!(f,"{}",line.norm_text(self.1))?;     
                    }
                }
            }
        }
        Ok(())
    }
}

impl<'i> Display for LineNormTextWrite<'i>{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self.0 {
            Line::Lyrics(m)=>{
                for evt in m.iter().flatten().flatten(){
                    if let LyricEvent::LyricText(evt) = evt{
                        write!(f,"{}",normalize_text(evt,self.1) )?;
                    }
                }
                write!(f," ")?;
                Ok(())
            },
            Line::Mixed(m)=>{
                for (_,block) in m.iter().flatten(){
                    for evt in block{
                        if let LyricEvent::LyricText(evt) = evt{
                            write!(f,"{}",normalize_text(evt,self.1) )?;
                        }
                    }
                }
                write!(f," ")?;
                Ok(())
            }
            _ => Ok(())
        }
    }
}
