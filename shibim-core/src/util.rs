use std::iter::FusedIterator;
use std::fmt::Write;
type SizeHint = (usize, Option<usize>);
#[inline]
pub fn add_scalar(sh: SizeHint, x: usize) -> SizeHint {
    let (mut low, mut hi) = sh;
    low = low.saturating_add(x);
    hi = hi.and_then(|elt| elt.checked_add(x));
    (low, hi)
}

/*adapted from itertools */
pub trait Itertools: Iterator {
    fn partition_result<A, B, F, L, R>(self, mut predicate: F) -> (A, B)
    where
        Self: Sized,
        F: FnMut(Self::Item) -> Result<L, R>,
        A: Default + Extend<L>,
        B: Default + Extend<R>,
    {
        let mut left = A::default();
        let mut right = B::default();

        self.for_each(|val| match predicate(val) {
            Ok(v) => left.extend(Some(v)),
            Err(v) => right.extend(Some(v)),
        });

        (left, right)
    }
    fn partition_result_triple<A, B, F, L, R>(self, mut predicate: F) -> (A, B)
    where
        Self: Sized,
        F: FnMut(Self::Item) -> Result<(L,Option<R>), R>,
        A: Default + Extend<L>,
        B: Default + Extend<R>,
    {
        let mut left = A::default();
        let mut right = B::default();

        self.for_each(|val| match predicate(val) {
            Ok((v,w)) => {
                left.extend(Some(v));
                if let Some(w) = w{
                    right.extend(Some(w))
                };
            }
                ,
            Err(v) => right.extend(Some(v)),
        });

        (left, right)
    }
    fn join(&mut self, sep: &str) -> String
        where Self::Item: std::fmt::Display
    {
        match self.next() {
            None => String::new(),
            Some(first_elt) => {
                // estimate lower bound of capacity needed
                let (lower, _) = self.size_hint();
                let mut result = String::with_capacity(sep.len() * lower);
                write!(&mut result, "{}", first_elt).unwrap();
                self.for_each(|elt| {
                    result.push_str(sep);
                    write!(&mut result, "{}", elt).unwrap();
                });
                result
            }
        }
    }
    fn dedup_by<Cmp>(self, cmp: Cmp) -> DedupBy<Self, Cmp>
        where Self: Sized,
              Cmp: FnMut(&Self::Item, &Self::Item)->bool,
    {
        dedup_by(self, cmp)
    }

}

macro_rules! clone_fields {
    ($($field:ident),*) => {
        #[inline] // TODO is this sensible?
        fn clone(&self) -> Self {
            Self {
                $($field: self.$field.clone(),)*
            }
        }
    }
}

#[must_use = "iterator adaptors are lazy and do nothing unless consumed"]
pub struct CoalesceBy<I, F, T>
where
    I: Iterator,
{
    iter: I,
    last: Option<T>,
    f: F,
}

impl<I: Clone, F: Clone, T: Clone> Clone for CoalesceBy<I, F, T>
where
    I: Iterator,
{
    clone_fields!(last, iter, f);
}


pub trait CoalescePredicate<Item, T> {
    fn coalesce_pair(&mut self, t: T, item: Item) -> Result<T, (T, T)>;
}

impl<I, F, T> Iterator for CoalesceBy<I, F, T>
where
    I: Iterator,
    F: CoalescePredicate<I::Item, T>,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        // this fuses the iterator
        let last = self.last.take()?;

        let self_last = &mut self.last;
        let self_f = &mut self.f;
        Some(
            self.iter
                .try_fold(last, |last, next| match self_f.coalesce_pair(last, next) {
                    Ok(joined) => Ok(joined),
                    Err((last_, next_)) => {
                        *self_last = Some(next_);
                        Err(last_)
                    }
                })
                .unwrap_or_else(|x| x),
        )
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let (low, hi) = add_scalar(self.iter.size_hint(), self.last.is_some() as usize);
        ((low > 0) as usize, hi)
    }

    fn fold<Acc, FnAcc>(self, acc: Acc, mut fn_acc: FnAcc) -> Acc
    where
        FnAcc: FnMut(Acc, Self::Item) -> Acc,
    {
        if let Some(last) = self.last {
            let mut f = self.f;
            let (last, acc) = self.iter.fold((last, acc), |(last, acc), elt| {
                match f.coalesce_pair(last, elt) {
                    Ok(joined) => (joined, acc),
                    Err((last_, next_)) => (next_, fn_acc(acc, last_)),
                }
            });
            fn_acc(acc, last)
        } else {
            acc
        }
    }
}

impl<I: Iterator, F: CoalescePredicate<I::Item, T>, T> FusedIterator for CoalesceBy<I, F, T> {}


pub type DedupBy<I, Pred> = CoalesceBy<I, DedupPred2CoalescePred<Pred>, <I as Iterator>::Item>;

#[derive(Clone)]
pub struct DedupPred2CoalescePred<DP>(DP);

pub trait DedupPredicate<T> {
    // TODO replace by Fn(&T, &T)->bool once Rust supports it
    fn dedup_pair(&mut self, a: &T, b: &T) -> bool;
}

impl<DP, T> CoalescePredicate<T, T> for DedupPred2CoalescePred<DP>
where
    DP: DedupPredicate<T>,
{
    fn coalesce_pair(&mut self, t: T, item: T) -> Result<T, (T, T)> {
        if self.0.dedup_pair(&t, &item) {
            Ok(t)
        } else {
            Err((t, item))
        }
    }
}

impl<T, F: FnMut(&T, &T) -> bool> DedupPredicate<T> for F {
    fn dedup_pair(&mut self, a: &T, b: &T) -> bool {
        self(a, b)
    }
}

/// Create a new `DedupBy`.
pub fn dedup_by<I, Pred>(mut iter: I, dedup_pred: Pred) -> DedupBy<I, Pred>
where
    I: Iterator,
{
    DedupBy {
        last: iter.next(),
        iter,
        f: DedupPred2CoalescePred(dedup_pred),
    }
}



impl<I: Iterator> Itertools for I {}
type RefIterIm<'i, T> = Box<dyn Iterator<Item = &'i T> + 'i>;

/*A bandaid because I couldn't get Markup to accept iterators */
pub trait RefIter {
    type Item;
    fn ref_iter(&self) -> RefIterIm<'_, Self::Item>;
}
/*
struct RefIterFn<'i, C, T: 'i,U :'i, F>
where
    C: RefIter<Item = T>,
    F: Fn(RefIterIm<'i, T>) -> RefIterIm<'i, U>,
{
    container: &'i C,
    fun: F,
}
*/
impl<T> RefIter for Vec<T> {
    type Item = T;
    fn ref_iter(&self) -> RefIterIm<'_, Self::Item> {
        Box::new(self.iter())
    }
}
/*
impl<'i, C, T: 'i, U :'i, F> RefIter for RefIterFn<'i, C, T, U, F>
where
    C: RefIter<Item = T>,
    F: Fn(RefIterIm<'i, T>) -> RefIterIm<'i, U>,
{
    type Item = U;
    fn ref_iter(&'i self) -> RefIterIm<'i, Self::Item> {
        Box::new((self.fun)(self.container.ref_iter()))
    }
}

fn and_iter<T>(container: impl RefIter<Item = T>, fun: impl FnMut()) {}
*/

