use crate::base::*;
use crate::error::{LoadErrors, ParseOrIOError, VisitorError, FileError};
use crate::util::Itertools;
use std::borrow::{Borrow, Cow};
use std::collections::HashMap;
use std::fs;
use std::path::{Path, PathBuf};
use std::time::Duration;

pub fn read_shb(file_name: &Path) -> Result<Song, LoadErrors<SHBParseError>> {
    //println!("{}",file_name.to_string_lossy());
    //let session = shibim_base::SongSessionInfo::new(file_name);
    let u = fs::read_to_string(file_name);
    let u = u.map_err(|e| LoadErrors {
        file: file_name.to_string_lossy().to_string(),
        detail: ParseOrIOError::IOError(e),
    });
    u.and_then(|s| {
        let (song, errs) = crate::parser::parse_shb(&s);
        if !errs.is_empty() {
            Err(LoadErrors {
                file: file_name.to_string_lossy().to_string(),
                detail: ParseOrIOError::ParseError(errs),
            })
        } else {
            Ok(song)
        }
    })
}

pub fn try_read_shb(file_name: &Path) -> Result<(Song,Option<LoadErrors<SHBParseError>>),LoadErrors<SHBParseError>> {
    //println!("{}",file_name.to_string_lossy());
    //let session = shibim_base::SongSessionInfo::new(file_name);
    let u = fs::read_to_string(file_name);
    let u = u.map_err(|e| FileError {
        file: file_name.to_string_lossy().to_string(),
        detail: ParseOrIOError::IOError(e),
    });
    u.and_then(|s| {
        
        let (song, errs) = crate::parser::parse_shb(&s);
        Ok((
            song,
            if errs.is_empty(){
                None
            }else{
                Some(
                    FileError {
                        file: file_name.to_string_lossy().to_string(),
                        detail: ParseOrIOError::ParseError(errs),
                    }
                )
            }
        ))
            
    })
}


pub fn read_lst(file_name: &Path) -> Result<Songlist, LoadErrors<LSTParseError>> {
    let string_data = fs::read_to_string(file_name).map_err(|err| LoadErrors::<LSTParseError> {
        file: file_name.to_string_lossy().to_string(),
        detail: ParseOrIOError::IOError(err),
    })?;
    let (list, errors) = crate::parser::parse_lst(&string_data);
    if errors.is_empty() {
        Ok(list)
    } else {
        Err(LoadErrors {
            file: file_name.to_string_lossy().to_string(),
            detail: ParseOrIOError::ParseError(errors),
        })
    }
}

pub fn try_read_lst(file_name: &Path) -> Result<(Songlist,Option<LoadErrors<LSTParseError>>), LoadErrors<LSTParseError>> {
    let string_data = fs::read_to_string(file_name).map_err(|err| LoadErrors::<LSTParseError> {
        file: file_name.to_string_lossy().to_string(),
        detail: ParseOrIOError::IOError(err),
    })?;
    let (list, errors) = crate::parser::parse_lst(&string_data);
    Ok((list,
        if errors.is_empty(){
            None
        }else{
            Some(
                LoadErrors {
                    file: file_name.to_string_lossy().to_string(),
                    detail: ParseOrIOError::ParseError(errors),
                }
            )
        }
    ))
}
pub fn get_dir_filelist_ext(
    dir: impl AsRef<Path>,
    req_ext: &str,
) -> Result<impl Iterator<Item = PathBuf> + '_, std::io::Error> {
    let dir = fs::read_dir(dir)?;
    //TODO: ignore unreadable files like this?
    Ok(dir.filter_map(move |entry| {
        let entry = entry.ok()?;
        let path = entry.path();
        if path.extension()? == req_ext {
            Some(path)
        } else {
            None
        }
    }))
}

pub fn load_batch<I, F, T, E>(
    files: I,
    mut load_fn: F,
    prefix: &str,
) -> (Vec<CacheItem<T>>, Vec<LoadErrors<E>>)
where
    I: IntoIterator,
    I::Item: Borrow<Path>,
    F: FnMut(&Path) -> Result<T, LoadErrors<E>>,
{
    files
        .into_iter()
        .map(|path| (path.borrow().to_owned(), load_fn(path.borrow())))
        .partition_result(|(path, content)| {
            content.map(|list| {
                let path: &Path = path.borrow();
                CacheItem::<T> {
                    file_name: path.to_owned(),
                    handle: prefix.to_owned() + path.file_stem().and_then(|s| s.to_str()).unwrap(),
                    modified: get_timestamp(path.borrow()),
                    data: list,
                }
            })
        })
}

pub fn load_batch_tolerant<I, F, T, E>(
    files: I,
    mut load_fn: F,
    prefix: &str,
) -> (Vec<CacheItem<T>>, Vec<LoadErrors<E>>)
where
    I: IntoIterator,
    I::Item: Borrow<Path>,
    F: FnMut(&Path) -> Result<(T,Option<LoadErrors<E>>), LoadErrors<E>>,
{
    files
        .into_iter()
        .map(|path| (path.borrow().to_owned(), load_fn(path.borrow())))
        .partition_result_triple(|(path, content)| {
            content.map(|(list,parse_errors)| {
                let path: &Path = path.borrow();
                (CacheItem::<T> {
                    file_name: path.to_owned(),
                    handle: prefix.to_owned() + path.file_stem().and_then(|s| s.to_str()).unwrap(),
                    modified: get_timestamp(path.borrow()),
                    data: list,
                }, parse_errors)
            })
        })
}

fn get_timestamp(path: &Path) -> Option<Duration> {
    let metadata = fs::metadata(path).ok()?;
    let systime = metadata.modified().ok()?;
    systime.duration_since(std::time::UNIX_EPOCH).ok()
}

#[derive(Debug)]
pub struct SHBBatchResults {
    pub songs: Vec<(PathBuf, Song)>,
    pub errors: Vec<LoadErrors<SHBParseError>>,
    pub names: HashMap<String, usize>,
}
#[derive(Debug, Clone)]
pub struct CacheItem<T> {
    pub file_name: PathBuf,
    pub modified: Option<Duration>,
    pub handle: String,
    pub data: T,
}

trait SongVisitor {
    fn process(&mut self, e: &Song) -> Result<(), VisitorError>;
}
#[derive(Clone, Copy)]
pub enum ResourceType {
    LST,
    SHB,
    STATIC,
}

pub trait PathsRef {
    fn source(&self, path: impl AsRef<Path>, kind: ResourceType) -> PathBuf;
    fn output(&self, path: impl AsRef<Path>, kind: ResourceType) -> PathBuf;
    fn rel_out(
        &self,
        path: impl AsRef<Path>,
        dst_kind: ResourceType,
        src_kind: ResourceType,
    ) -> String;
    fn output_dir(&self, res: ResourceType) -> &Path;
    fn source_dir(&self, res: ResourceType) -> &Path;
    fn static_preloaded(&self, path: impl AsRef<Path>) -> &str;
    fn static_preload(&mut self, path: impl AsRef<Path>) -> Result<(), std::io::Error>;
    fn make_output_dirs(&self) -> std::io::Result<()> {
        std::fs::create_dir_all(self.output_dir(ResourceType::SHB))?;
        std::fs::create_dir_all(self.output_dir(ResourceType::LST))?;
        Ok(())
    }
    fn make_input_dirs(&self) -> std::io::Result<()> {
        std::fs::create_dir_all(self.source_dir(ResourceType::SHB))?;
        std::fs::create_dir_all(self.source_dir(ResourceType::LST))?;
        Ok(())
    }
    fn copy_static_assets(&self) -> Result<(), Vec<std::io::Error>> {
        copy_dir(
            self.source_dir(ResourceType::STATIC),
            self.output_dir(ResourceType::STATIC),
        )
    }
    fn default_song_iter(&self) {}
}

pub struct DefaultPathConf {
    lst_src_dir: PathBuf,
    shb_src_dir: PathBuf,
    lst_out_dir: PathBuf,
    shb_out_dir: PathBuf,
    lst_rel_to_top: PathBuf,
    shb_rel_to_top: PathBuf,
    static_rel_to_top: PathBuf,
    lst_rel_from_top: PathBuf,
    shb_rel_from_top: PathBuf,
    static_rel_from_top: PathBuf,
    static_src_dir: PathBuf,
    static_out_dir: PathBuf,
    preloaded_data: HashMap<PathBuf, String>,
}
/*
* Output_dir
* - js
* - css
* - list
* - - song1.html
* - - song2.html
* - song
* - - list1.html
* - - list2.html
*/
impl DefaultPathConf {
    pub fn new(
        lst_src: impl AsRef<Path>,
        shb_src: impl AsRef<Path>,
        static_src: impl AsRef<Path>,
        out_dir: impl AsRef<Path>,
    ) -> Self {
        DefaultPathConf {
            lst_src_dir: lst_src.as_ref().to_owned(),
            shb_src_dir: shb_src.as_ref().to_owned(),
            lst_out_dir: out_dir.as_ref().join("list/"),
            shb_out_dir: out_dir.as_ref().join("song/"),
            lst_rel_to_top: PathBuf::from("../"),
            shb_rel_to_top: PathBuf::from("../"),
            static_rel_to_top: PathBuf::from("./"),
            lst_rel_from_top: PathBuf::from("list/"),
            shb_rel_from_top: PathBuf::from("song/"),
            static_rel_from_top: PathBuf::from("./"),
            static_src_dir: static_src.as_ref().to_owned(),
            static_out_dir: out_dir.as_ref().to_owned(),
            preloaded_data: HashMap::new(),
        }
    }
}

impl PathsRef for DefaultPathConf {
    fn source(&self, path: impl AsRef<Path>, kind: ResourceType) -> PathBuf {
        use ResourceType::*;
        let base_path = match kind {
            LST => &self.lst_src_dir,
            SHB => &self.shb_src_dir,
            STATIC => &self.static_src_dir,
        };
        base_path.join(path)
    }
    fn output(&self, path: impl AsRef<Path>, kind: ResourceType) -> PathBuf {
        let base_path = self.output_dir(kind);
        base_path.join(path)
    }

    fn output_dir(&self, t: ResourceType) -> &Path {
        use ResourceType::*;
        match t {
            LST => &self.lst_out_dir,
            SHB => &self.shb_out_dir,
            STATIC => &self.static_out_dir,
        }
    }

    fn source_dir(&self, t: ResourceType) -> &Path {
        use ResourceType::*;
        match t {
            LST => &self.lst_src_dir,
            SHB => &self.shb_src_dir,
            STATIC => &self.static_src_dir,
        }
    }

    fn rel_out(
        &self,
        path: impl AsRef<Path>,
        dst_kind: ResourceType,
        src_kind: ResourceType,
    ) -> String {
        use ResourceType::*;
        let to_top = match src_kind {
            LST => &self.lst_rel_to_top,
            SHB => &self.shb_rel_to_top,
            STATIC => &self.static_rel_to_top,
        };
        let from_top = match dst_kind {
            LST => &self.lst_rel_from_top,
            SHB => &self.shb_rel_from_top,
            STATIC => &self.static_rel_from_top,
        };
        to_top
            .join(from_top)
            .join(path)
            .to_string_lossy()
            .to_string()
    }

    fn static_preload(&mut self, path: impl AsRef<Path>) -> Result<(), std::io::Error> {
        self.preloaded_data.insert(
            path.as_ref().to_owned(),
            fs::read_to_string(self.static_src_dir.join(path))?,
        );
        Ok(())
    }
    fn static_preloaded(&self, path: impl AsRef<Path>) -> &str {
        //THIS will panic if the file was not preloaded
        //Because handling errors in the html templates
        //is ugly/unwise; and that for some cases, we need
        //to have files in memory to insert them in the html,
        //I'm left with this ugly 'solution'
        self.preloaded_data.get(path.as_ref()).unwrap()
    }
}

pub fn copy_dir(src: impl AsRef<Path>, dst: impl AsRef<Path>) -> Result<(), Vec<std::io::Error>> {
    let mut dir_stack = vec![Some(src.as_ref().to_owned())];
    let mut dst_stack = dst.as_ref().to_owned();
    let mut errors = Vec::new();
    //let mut errors = Vec::new();
    let mut is_top = true;
    while !dir_stack.is_empty() {
        let dir_path = dir_stack.pop().unwrap();
        if let Some(dir_path) = dir_path {
            dir_stack.push(None);
            let dir = fs::read_dir(&dir_path).unwrap();
            if !is_top {
                dst_stack.push(dir_path.file_name().unwrap());
            } else {
                is_top = false;
            }
            for entry in dir {
                let e_status =
                    entry
                        .and_then(|x| x.file_type().map(|y| (x, y)))
                        .and_then(|(entry, ftype)| {
                            let new_path = dst_stack.join(entry.file_name());
                            if ftype.is_dir() {
                                dir_stack.push(Some(entry.path()));
                                if !new_path.is_dir() {
                                    println!("Creating dir {}", new_path.to_string_lossy());
                                    fs::create_dir(new_path)
                                } else {
                                    Ok(())
                                }
                            } else if ftype.is_file() {
                                println!(
                                    "Copying file {} to {}",
                                    entry.path().display(),
                                    dst_stack.join(entry.file_name()).to_string_lossy()
                                );
                                fs::copy(entry.path(), new_path).map(|_| ())
                            } else {
                                //Is this possible?
                                eprintln!("Unexpected non-file non-directory item.");
                                Err(std::io::Error::new(std::io::ErrorKind::Other, ""))
                            }
                        });
                if let Err(ioerr) = e_status {
                    errors.push(ioerr);
                }
            }
        } else {
            dst_stack.pop();
        }
    }
    if errors.is_empty() {
        Ok(())
    } else {
        Err(errors)
    }
}

pub fn song_cache_to_index<'i>(
    cache_song: &'i CacheItem<Song>,
    paths: &impl PathsRef,
) -> SongIndexEntry<'i> {
    let mut rel_path = paths.rel_out(&cache_song.handle, ResourceType::SHB, ResourceType::STATIC);
    rel_path += ".html";
    SongIndexEntry {
        name: Cow::Borrowed(&cache_song.data.name),
        subtitle: cache_song
            .data
            .metadata
            .get("subtitle")
            .map(|x| Cow::Borrowed(x.as_str())),
        tonality: cache_song.data.tonality,
        href: rel_path.into(),
        section_names: cache_song
            .data
            .sections
            .iter()
            .filter_map(|sblock|sblock.section())
            .map(|sec: &Section| sec.name.clone() + "-" + &sec.description)
            .collect(),
    }
}

pub fn list_cache_to_index<'i>(cache_list : &'i CacheItem<Vec<CompiledSong>>, paths : &impl PathsRef)->SonglistIndexEntry<'i>{
    SonglistIndexEntry {
        name: Cow::Borrowed(&cache_list.handle),
        songs: cache_list
            .data
            .iter()
            .map(|c_song| -> Cow<'_, str> { Cow::Borrowed(&c_song.name) })
            .collect(),
        href: paths
            .rel_out(
                cache_list.handle.clone() + ".html",
                ResourceType::LST,
                ResourceType::STATIC,
            )
            .into(),
    }
}