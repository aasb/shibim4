use markup;
use crate::base as base;
use crate::i18n::*;
use crate::toneutil;
use crate::files::{PathsRef, ResourceType};
use crate::util;
/* 
struct SongHTMLOptions{
    use_header : bool,
    remove_edit_buttons : bool,
    force_flats : bool
}*/
/*
fn get_orders_serialized(song : & base::Song)->String{
    serde_json::to_string(
        &song.orders.keys()
        .collect::<Vec<&String>>()
    )
    .unwrap_or_default()
}*/

fn get_default_order(song : &base::Song)->Option<(&str, &Vec<usize>)>{
    song.orders.get_key_value("default")
    .or_else(
        || song.orders.get_key_value("full")
    ).or_else(
        || song.orders.iter().next()
    ).map(|(k,v)|(k.as_str(),v))
}
/*
fn create_default_order(song : &base::Song)->Vec<usize>{
   (0..song.sections.len()).collect()
}*/
markup::define!(
    JsSrc<'i,P>(name : &'i str, prov : &'i P, src_type : ResourceType, static_js : bool)
    where P : PathsRef{
        @if *static_js{
            script{
                {markup::raw(prov.static_preloaded(name))}
                ';'
            }
        }else{
            script {
                //We need to be able to read the script as text
                //to save the whole page
                {markup::raw(format!("
                (()=>{{
                    let script = document.currentScript;
                    document.addEventListener('DOMContentLoaded',async ()=>{{
                        let res = await fetch(\"{}\");
                        if (res.ok) {{
                            let s = document.createElement(\"script\");
                            s.textContent = await res.text();
                            document.body.appendChild(s);
                            script.remove();
                        }}else{{
                            console.log('Error fetching script, status '+res.status);
                        }}
                }}) }})();",prov.rel_out(name,ResourceType::STATIC,*src_type)))}
            }
        }
    }
    
    CssSrc<'i,P>(name : &'i str, prov : &'i P, src : ResourceType, static_css : bool)
    where P : PathsRef{
        @if *static_css{
            style{
                {markup::raw(prov.static_preloaded(name))}
            }
        }else{
            link [rel="stylesheet", "type"="text/css", href=prov.rel_out(name,ResourceType::STATIC,*src)];
        }
    }

    SongPage<'i,P>(song : &'i base::Song, prov : &'i P, static_js : bool, lang : Lang)
    where P : PathsRef{
        {markup::doctype()}
        html{
            head{
                meta [charset ="utf-8"];
                meta [name="viewport", content="width=device-width"];
                {CssSrc{name : "css/fonts.css", prov : *prov, src : ResourceType::SHB,static_css : *static_js}}
                {CssSrc{name : "css/song.css", prov : *prov, src : ResourceType::SHB,static_css : *static_js}}
                {CssSrc{name : "css/song_page.css", prov : *prov, src : ResourceType::SHB,static_css : *static_js}}
            }
            body{
                {JsSrc{
                    name : "js/song.js",
                    prov : *prov,
                    src_type : ResourceType::SHB,
                    static_js : *static_js
                }}
                {ConfButtons{lang : *lang, paths : *prov}}
                div . "u-container"{
                    {Song{song}}
                }
                div . "navbar-space"{

                }
            }
        }
    }
    
    SonglistPage<'i,P>(list : &'i Vec<base::CompiledSong>, prov : &'i P, static_js : bool, lang : Lang)
    where P : PathsRef{
        {markup::doctype()}
        html{
            head{
                meta [charset ="utf-8"];
                meta [name="viewport", content="width=device-width"];
                {CssSrc{name : "css/song.css", prov : *prov, src : ResourceType::LST,static_css : *static_js}}
                {CssSrc{name : "css/song_page.css", prov : *prov, src : ResourceType::SHB,static_css : *static_js}}
                {CssSrc{name : "css/fonts.css", prov : *prov, src : ResourceType::LST,static_css : *static_js}}
            }
            body{
                {ConfButtons{lang : *lang, paths : *prov}}
                {JsSrc{
                    name : "js/song.js",
                    prov : *prov,
                    src_type : ResourceType::LST,
                    static_js : *static_js
                }}
                {Songlist{list}}
                div . "navbar-space"{

                }
            }
        }
    }

    Songlist<'i>(list : &'i Vec<base::CompiledSong>){
        div . "u-container"{
            @for (i,song) in list.iter().enumerate(){
                {CompiledSong{song : song.into(),id: Some(i as u64)}}
            }
        }
    }
    Song<'i>(song: &'i base::Song){
        article . "u-song" [
                "data-tonic" = song.tonality.tonic,
                "data-otonic" = song.tonality.tonic,
                //"data-orders" = get_orders_serialized(song), /* back to the drawing board with this feature */
                "data-mode" = {
                    if let base::TonicKind::Minor = song.tonality.kind{
                        "m"
                    }else{
                        ""
                    }
                },
                "data-nsections" = song.sections.len(),
                "data-hidecontrol"
            ]{
            {SongHeader{name : &song.name, tonality : song.tonality}}
            @if let Some((_name,indexes)) = get_default_order(song){
                {SongOrder{
                    sections : &song.sections,
                    order : indexes,
                    use_flats: toneutil::get_default_use_flat(song.tonality)
                }}
            }else{
                @for sblock in &song.sections{
                    {SongBlock{sblock, use_flats : toneutil::get_default_use_flat(song.tonality)}}
                }
            }
        }

    }
    
    CompiledSong<'i>(song: base::SongRef<'i>, id : Option<u64>){
        article . "u-song"[
                "data-tonic" = song.tonality.tonic,
                "data-otonic" = song.tonality.tonic,
                "data-mode" = {
                    if let base::TonicKind::Minor = song.tonality.kind{
                        "m"
                    }else{
                        ""
                    }
                },
                "data-song-id" = {
                    if let Some(id) = id{
                        format!("{:X}",id)
                    }else{
                        "".to_string()
                    }
                },
                "data-nsections" = song.sections.len(),
                "data-hidecontrol"
            ]{
            @if !song.headless{
                {SongHeader{name : song.name, tonality : song.tonality}}
            }
            @for sblock in  song.sections{
                {SongBlock{sblock,use_flats : toneutil::get_default_use_flat(song.tonality)}}
            }
        }
    }
    SongHeader<'i>(name : &'i str, tonality : base::Tonality){
        $ "u-title-box"{
            @if !name.is_empty(){
                $ "u-song-name"{
                    h2 {{name}}
                }
            }
            {ToneButton{tonality: *tonality}}
            {UtilButtonsHTML{}}
        }
    }
    ToneButton(tonality : base::Tonality){
        button . "tone-button" {
                {ChordRoot{root : tonality.tonic, use_flats : toneutil::get_default_use_flat(*tonality)}}
                @if let base::TonicKind::Minor = tonality.kind{
                    $"u-n"{"m"}
                }
        }
    }
    SongOrder<'i,'b>(sections : &'i Vec<base::SongBlock>,order : &'b Vec<usize>,use_flats : bool){
        @for may_section in order.iter().map(|x|sections.get(*x)){
            @if let Some(base::SongBlock::Section(section)) = may_section{
                {Section{section, use_flats : *use_flats} }
            }else if let Some(base::SongBlock::Annotation(s)) = may_section{
                //TODO:
                {eprintln!("Unimplemented compiled annotation!");""}
            }else{
                {eprintln!("Corrupted section information!");""} //Todo
            }
        }
    }
    
    SongBlock<'i>(sblock : &'i base::SongBlock, use_flats : bool){
        @match sblock{
            base::SongBlock::Section(section) => {
                {Section{section : section,use_flats : *use_flats}}
            }
            base::SongBlock::Annotation(annotation) => {
                $ "u-annotation" {
                    {annotation}
                }
            }
            
        }
    }
    
    Section<'i>(section : &'i base::Section, use_flats : bool){
        @if let Some(tonality) = section.local_tonality{
            $ "u-section" [
                "data-id" = &section.name,
                "data-tonic" = tonality.tonic,
                "data-mode" = {
                        if let base::TonicKind::Minor = tonality.kind{
                            "m"
                        }else{
                            ""
                        }
                    }
                ]
            {
                $ "u-title-box"{
                    $ "u-title-background"{
                        $ "u-section-name"{
                            {&section.name}
                        }
                        h3{
                            {&section.description}  
                        }
                        @if section.name.starts_with('C') {
                            $"u-chorus-mark"{}
                        }
                    }
                    {ToneButton{tonality}}
                    {UtilButtonsHTML{}}
                }
                @for subsection in &section.subsections{
                    {Subsection{subsecion : subsection, use_flats : *use_flats}}
                }
            }
        }else{
            $ "u-section" ["data-id" = &section.name]
            {
                $ "u-title-box"{
                    $ "u-title-background"{
                        $ "u-section-name"{
                            {&section.name}
                        }
                        h3{
                            {&section.description}  
                        }
                        @if section.name.starts_with('C') {
                            $"u-chorus-mark"{}
                        }
                    }
                    {UtilButtonsHTML{}}
                }
                @for subsection in &section.subsections{
                    {Subsection{subsecion : subsection, use_flats : *use_flats}}
                }
            }
        }
    }
    Subsection<'i>(subsecion : &'i base::Subsection, use_flats : bool){
        $ "u-s"{
            @if let Some(re) = subsecion.metadata.get("ref") {
                $"u-ref"{
                    {re}
                }
            }
            @for line in &subsecion.lines{
                {Line{line, use_flats : *use_flats}}
            }
        }
    }
    Line<'i>(line : &'i base::Line, use_flats : bool){
        @match line {
            base::Line::Lyrics(lyrics)=>{
                $"u-xl" {
                @for measure in lyrics{
                    $"u-m"{
                        @for block in measure{
                            $"u-b"{
                            $"u-l"{
                           
                                @for fragment in block{
                                    {LyricEvent{evt : fragment}}
                                }
                            }
                            }
                        }
                    }
                }
                }
            }
            base::Line::Chords(chords)=>{
                @let mut on_parens = false;
                $"u-xc" {
                @for measure in chords{
                    $"u-m"{
                        @for block in measure{
                            $"u-b"{
                                $"u-c"{
                                @for fragment in block{
                                    @if let base::MusicEvent::OpenParen = fragment{
                                        {on_parens = true;""}
                                    }else if let base::MusicEvent::CloseParen = fragment{
                                        {on_parens = false;""}
                                    }
                                    {MusicEvent{evt : fragment, use_flats : *use_flats, is_par: on_parens}}
                                }
                                }
                            }
                        }
                    }
                }
                }
            }
            base::Line::Mixed(elems)=>{
                @let mut on_parens = false;
                $"u-x" {
                @for measure in elems{
                    $"u-m"{
                        @for (chord_block,lyric_block) in measure{
                            $"u-b"{
                                @if chord_block.is_empty(){
                                    $"u-ce"{}
                                }else{
                                    $"u-c"{
                                        @for evt in chord_block{
                                            @if let base::MusicEvent::OpenParen = evt{
                                                {on_parens = true;""}
                                            }else if let base::MusicEvent::CloseParen = evt{
                                                {on_parens = false;""}
                                            }
                                            {MusicEvent{evt, use_flats : *use_flats,is_par : on_parens}}
                                        }
                                    }
                                }
                                $"u-l"{
                                    @for evt in lyric_block{
                                        {LyricEvent{evt}}
                                    }
                                }
                            }
                        }
                        }
                }
                }
            }
        }
    }

    TimeOffset<'i>(time : &'i base::TimeOffset){
        $ "u-tim"{
            @if time.beat == -1{
                @if time.den == 2 && time.num == 1{
                    {"<"}
                }else{
                    span {
                        "-"
                        sup {{time.num}}
                        sub {{time.den}}
                    }
                }
            }else{
                { time.beat }

                @if time.den == 0 || time.num == 0{

                }else if time.den == 2 && time.num == 1{
                    {"\""}
                }else{
                    span {
                        sup {{time.num}}
                        sub {{time.den}}
                    }
                }
            }
        }
    }
    
    ChordRoot(root : base::NoteHeight, use_flats : bool){
        @if toneutil::is_altered(*root){
            @if *use_flats{
                {base::FLAT_TONIC_NAMES[*root as usize]}
                $"u-a" {
                    "b"
                }
            }else{
                {base::SHARP_TONIC_NAMES[*root as usize]}
                
                $"u-a"{
                    "#"
                }
            }
        }else{
            {base::SHARP_TONIC_NAMES[*root as usize]}
        }
    }


    ChordEvent<'i>(chord : &'i base::ChordEvent, use_flats : bool){
    
        @if let Some(time) = &chord.time {

                {TimeOffset{time}}
        }

        $ "u-r" {
            {ChordRoot{root : chord.root,use_flats : *use_flats}}
        }

        @if let base::ChordKind::Minor = chord.kind{
            $ "u-n"{"m"}
        }
        @for modifier in &chord.modifiers{
            {ChordModifier{modifier}}
        }
        @if let Some(bass) = chord.bass{
            $"u-bas" {
                    "/"
                    $ "u-r" {
                    {ChordRoot{root : bass, use_flats: *use_flats}}
                } 
            }
        }
    }

    ChordModifier<'i>(modifier : &'i base::ChordModifier){
        @match modifier{
            base::ChordModifier::Keyword(keyword) =>{
                @match keyword{
                    base::ChordKeyword::Maj |
                     base::ChordKeyword::K5 |
                     base::ChordKeyword::K6 |
                     base::ChordKeyword::K7 |
                     base::ChordKeyword::K9 | 
                     base::ChordKeyword::K13 => {
                        //Short for keyword
                        $"u-k" {{CHORD_KEYWORDS_NAMES[keyword]}}
                    }
                    _=>{
                        //Short for keyword-long
                        $"u-kl" {{CHORD_KEYWORDS_NAMES[keyword]}}
                    }
                }
            }
            base::ChordModifier::Alteration(alter)=>{
                $ "u-alt"  {
                    @match alter.kind {
                        base::ChordAlterationKind::Flat =>{"b"}
                        base::ChordAlterationKind::Sharp => {"#"}
                        base::ChordAlterationKind::No => {"no"}
                    }
                    {alter.degree}                   
                }
                
            }
        }
    }
    LyricEvent<'i>(evt: &'i base::LyricEvent){
        @match evt{
            base::LyricEvent::LyricText(text)=>{
                {text}
            }
            base::LyricEvent::LyricBreak=>{
                $"u-lbrk"{
                    "\u{200B}"
                }
            }
        }
    }
    MusicEvent<'i>(evt : &'i base::MusicEvent, use_flats : bool, is_par : bool){
        @match evt{
            base::MusicEvent::ChordEvent(evt)=>{
                @if !*is_par{
                    $ "u-h"{
                        {ChordEvent{chord : evt, use_flats : *use_flats}}
                    }
                }else{
                    $ "u-h" . "p"{
                        {ChordEvent{chord : evt, use_flats : *use_flats}}
                    }
                }
            }
            base::MusicEvent::MelodyEvent(evt)=>{
                $ "u-mel"{
                    {MelodyEvent{melody:evt, use_flats : *use_flats}}
                }
            }
            base::MusicEvent::RepeatMeasure=>{
                $ "u-sym" . "rept"{
                    {"/"} //TODO: ooo
                }
            }
            base::MusicEvent::Annotation(text)=>{
                $ "u-ann"{
                    {text}
                }
            }
            base::MusicEvent::NumberedMeasure(num)=>{
                $ "u-num"{
                    {num}
                }
            }
            base::MusicEvent::OpenParen=>{
                $ "u-sym" . "opp"{
                    "("
                }
            }
            base::MusicEvent::CloseParen=>{
                $ "u-sym" . "clp"{
                    ")"
                }
            }
            _=>{
                {eprintln!("Not implemented {:?}!",evt);""} //TODO ooo
            }
        }
    }


    MelodyEvent<'i>(melody : &'i Vec<base::NoteHeight>, use_flats : bool){

        @for (i,note) in melody.iter().enumerate(){
            $"u-r"{
                {ChordRoot{root:*note,use_flats : *use_flats}} 
            }
            {if i < melody.len() - 1 {" "} else {""} }
        }
    }

    UtilButtonsHTML(){
        button . "collapse-button" {

        }
        button ."moveup-button"{

        }
        button . "movedown-button"{

        }
        button . "remove-button" [style="display:none;"]{

        }
    }

    HeadHTML(){
        head{
            meta [charset ="utf-8"];
            meta [name="viewport", content="width=device-width"];
            link [rel="stylesheet", "type"="text/css", href="../css/style.css"];
        }
    }

    HeadContentIndexHTML(){
        meta [charset ="utf-8"];
        meta [name="viewport", content="width=device-width"];
        link [rel="stylesheet", "type"="text/css", href="css/index.css"];
    }

    ConfButtons<'i,P>(lang : Lang, paths : &'i P)
    where P : PathsRef{ //TODO: implement language
        div # "tool-bar-pos" {
            button . "cog-bg" # "settings-button"{}
            div . "conf-bar" # "nav-bar" [style="display:none"]{
                div . "conf-section"{
                    span . "conf-label"{
                        "Ver:"
                    }
                    button ."conf-btn" # "hidechord-btn" {"Acordes"}
                    button ."conf-btn" # "hidelyric-btn" {"Letra"}
                    button ."conf-btn" # "hidecontrol-btn" {"Edición"}
                    button ."conf-btn" # "margin-btn" {"Margen"}
                    button ."conf-btn" # "sharp-btn" {"b #"}
                }
                div . "conf-section"{
                    span . "conf-label"{
                        "Tamaño Letra:"
                    }
                    button ."conf-btn" # "small-btn" {"A-"}
                    button ."conf-btn" # "big-btn" {"A+"}
                }
                div . "conf-section"{
                    span . "conf-label"{
                        "Transponer:"
                    }
                    button ."conf-btn" # "tr-down-btn" {"-"}
                    span # "tr-current" {
                        "0"
                    }
                    button ."conf-btn" # "tr-up-btn" {"+"}
                }
                div . "conf-section"{
                    button ."conf-btn" # "save-btn"{"Descargar"}
                }
                //button ."conf-btn" # "col-btn"{"Columnas"}
                //button ."conf-btn" # "dark-btn" {"Color"}
                div . "conf-section"{
                    a . "conf-btn" # "index-btn" [href="../songs.html"] {"Índice cantos"}
                    a . "conf-btn" # "index-btn" [href="../songlists.html"] {"Índice listas"}
                    button . "conf-btn" # "copy-btn" { "Copiar texto" }
                    a . "conf-btn" # "pres-btn" [href="../present.html",target="_blank"] {"Presentacion"}
                }
                //button ."conf-btn" # "conn-btn" {"Conectar"}
            }
        }
    }
    SongIndex<'a>(list : Vec<base::SongIndexEntry<'a>>){
        {markup::doctype()}
        html{
            head{
                {HeadContentIndexHTML{}}
            }
            body{
                {StaticNavBarHTML{}}
                ul . "i-songindex"{
                    @for entry in list{
                        li{
                            {SongIndexEntry{entry}}
                        }
                    }
                }
            }
        }
    }
    SongIndexIter<I,F>(it_list : I,mod_fun : F)
    where I : util::RefIter<Item=base::Song>,
          F : Fn(&base::Song)->base::SongIndexEntry{
        @let list = it_list.ref_iter().map(mod_fun);
        {markup::doctype()}
        html{
            head{
                {HeadContentIndexHTML{}}
            }
            body{
                {StaticNavBarHTML{}}
                ul . "i-songindex"{
                    @for entry in list{
                        li{
                            {SongIndexEntry{entry : &entry}}
                        }
                    }
                }
            }
        }
    }

    SonglistIndex<'i>(list : Vec<base::SonglistIndexEntry<'i>>){
        {markup::doctype()}
        html{
            head{
                {HeadContentIndexHTML{}}
            }
            body{
                {StaticNavBarHTML{}}
                ul . "u-cardview"{
                    @for entry in list{
                        li{
                            {SonglistIndexEntry{entry}}
                        }
                    }
                }
            }
        }
    }


    SongIndexEntry<'i>(entry :  &'i base::SongIndexEntry<'i>){
        a [href=entry.href.as_ref()] {
            span . "i-sname"{
                {entry.name.as_ref()}
            }
            @if let Some(subtitle) = &entry.subtitle{
                span . "i-ssname"{
                    "("
                    {subtitle.as_ref()}
                    ")"
                }
            }
        }
        span . "i-tonality"{
            " ("
            {Tonality{tonality : entry.tonality}}
            ") "
        }
        div . "i-sections-box"{
            @for section in &entry.section_names{
                span . "i-section"{
                    {section} " "
                }
            }
        }
    }

    Tonality(tonality : base::Tonality){
        {ChordRoot{root : tonality.tonic, use_flats : toneutil::get_default_use_flat(*tonality)}}
        @if let base::TonicKind::Minor = tonality.kind{
            "m"
        }
    }

    SonglistIndexEntry<'i>(entry : &'i base::SonglistIndexEntry<'i>){
        a [href=&entry.href.as_ref()]{
            span . "u-lname"{
                {date::try_week_day_str(entry.name.as_ref())}
                {&entry.name.as_ref()}
            }
        }
        ul{
            @for song in &entry.songs{
                li{
                    {song.as_ref()}
                }
            }
        }
    }

    StaticNavBarHTML(){
        div .header{
            nav{
                a .navlink [href="songlists.html"]{"Listas"}
                a .navlink [href="songs.html"]{"Cantos"} 
                a .navlink [href="search.html"]{"Búsqueda"} 
            }
        }
    }

    SearchPage<'i,P>(prov : &'i P, lang : Lang)
    where P : PathsRef{
        {markup::doctype()}
        html{
            head{
                {HeadContentIndexHTML{}}
                script[src="js/minisearch.js","data-forget-save"]{}
                script[src="text.js","data-forget-save"]{}
                script[src="js/search.js","data-forget-save"]{}
                {JsSrc{
                    name : "js/song.js",
                    prov : *prov,
                    src_type : ResourceType::STATIC,
                    static_js : false
                }}
                link[rel="stylesheet", "type"="text/css", href="css/search.css"];
                link[rel="stylesheet", "type"="text/css", href="css/song.css"];
                link[rel="stylesheet", "type"="text/css", href="css/fonts.css"];
            }
            body{
                div ["data-forget-save"]{
                    {StaticNavBarHTML{}}
                }
                div # "top-container" ["data-forget-save"] {
                    div # "search-box" {
                        input # "search-text" ["type"="text"];
                    }
                    div # "search-results" {
                    }
                }
                div . "song-pad"{
                    {ConfButtons{lang : *lang, paths : *prov}}
                    div . "u-container"{

                    }
                }
                div . "navbar-space"{

                }
            }
        }
    }
    
    Test<P,F>(iterable : P, mod_fun : F)
    where P : util::RefIter<Item=u8>,
        F : Fn(u8)->char{
        @let iter = iterable.ref_iter();
        @for i in iter{
            {mod_fun(*i)}
        }
    }
);


//Do not want to add dependency for this non-essential
//one time 

mod date{
    fn is_leap(year : i64) -> bool{
        year % 4 == 0 &&
        (year % 100 != 0 ||
        year % 400 == 0)          
    }

    fn leap_count_1600(year : i64) -> i64{
        let delta = year - 1600;
        let leap_4 = (delta + 3)/4;
        let common_100 = (delta + 99)/100;
        let leap_400 = (delta + 399)/400;
        leap_4 - common_100 + leap_400
    }
    const ACC_DAYS_MONTH : [i64 ; 12] = 
      [ 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    //TODO: sad is to add spaces to string constants
    const DAY_NAMES: &[&str] = &["Dom ","Lun ","Mar ","Mie ","Jue ","Vie ","Sab "];

    fn day_count_1600_upto(year : i64) -> i64{
        (year - 1600)*365 + leap_count_1600(year)
    }

    fn parse_date(datestr : &str) -> Option<(i64,u8,u8)>{
        if datestr.len() < 10 {
            return None;
        }
        let year : i64  =(datestr.get(0..4)?).parse().ok()?;
        let month : u8  = (datestr.get(5..7)?).parse().ok()?;
        let day : u8 = (datestr.get(8..10)?).parse().ok()?;
        
        Some((year,month,day))
    }
    fn week_day(year : i64, month : u8, day : u8) -> Option<u8>{
        if year < 1600 || month > 12 || day > 31 || month < 1 || day < 1 {
            return None;
        }
        let day_ref = 6;
        let mut days_from_ref = day_count_1600_upto(year);
        days_from_ref += ACC_DAYS_MONTH[(month-1) as usize];
        if is_leap(year) && month > 2 {
            days_from_ref += 1;
        }
        days_from_ref = days_from_ref + (day as i64) - 1;
        Some( ((days_from_ref + day_ref) % 7) as u8)
    }

    pub fn try_week_day_str(datestr: &str) -> &str{
        let date = parse_date(datestr).and_then(|(y,m,d)|{
            week_day(y,m,d)
        });
        match date{
            None => "",
            Some(n) => DAY_NAMES[n as usize]
        }
    }

}