use std::borrow::Borrow;
use std::collections::HashMap;

use error::{FileError, LSTError, LSTLinkError, LSTLinkErrorKind};
use files::{load_batch_tolerant, try_read_lst, CacheItem};
use toneutil::Transposable;

extern crate lazy_static;
extern crate markup;
extern crate unicode_normalization;
pub mod base;
pub mod error;
pub mod files;
pub mod html;
pub mod i18n;
pub mod parser;
pub mod toneutil;
pub mod util;
use crate::base::*;
use crate::error::LoadErrors;
use crate::files::{get_dir_filelist_ext, load_batch, read_lst, read_shb};
use crate::toneutil::delta_tonality;
use std::path::Path;
#[derive(Default)]
pub struct SongMemoryDatabase {
    data: HashMap<String, CacheItem<Song>>,
}
type SongCache = Vec<CacheItem<Song>>;
type SongLoadErrors = Vec<LoadErrors<SHBParseError>>;
pub fn process_shb_dir(
    dir: &Path,
    prefix: &str,
) -> Result<(SongCache, SongLoadErrors), std::io::Error> {
    let paths = get_dir_filelist_ext(dir, "shb")?;
    Ok(load_batch(paths, read_shb, prefix))
}
pub fn process_lst_batch(
    paths: impl IntoIterator<Item = impl Borrow<Path>>,
    prefix: &str,
    resolver: &impl SongDatabase,
    tolerant: bool,
) -> (Vec<CacheItem<Vec<CompiledSong>>>, Vec<LSTError>) {
    let (loaded, errors) = if tolerant {
        load_batch_tolerant(paths, try_read_lst, prefix)
    } else {
        load_batch(paths, read_lst, prefix)
    };
    let mut errors: Vec<_> = errors.into_iter().map(LSTError::OpenError).collect();

    let linked: Vec<_> = loaded
        .into_iter()
        .filter_map(|entry| {
            let (songlist, mut link_errors) = resolver.compile_list(&entry.data);
            if link_errors.is_empty() || tolerant {
                Some(CacheItem {
                    file_name: entry.file_name,
                    modified: entry.modified,
                    handle: entry.handle,
                    data: songlist,
                })
            } else {
                errors.extend(
                    link_errors
                        .drain(..)
                        .map(|err| FileError {
                            file: entry.file_name.to_string_lossy().into(),
                            detail: err,
                        })
                        .map(LSTError::LinkError),
                );
                None
            }
        })
        .collect();
    (linked, errors)
}
pub trait SongDatabase {
    fn get_song(&self, id: &str) -> Option<&Song>;
    fn clear(&mut self);
    fn compile_entry(&self, entry: &SonglistEntry) -> Result<CompiledSong, LSTLinkErrorKind> {
        if let Some(song) = &entry.inline_data {
            //Maybe not clone?
            let mut out = song.clone();
            if let Some(tonic_to) = entry.tonic {
                let delta = delta_tonality(song.tonality, tonic_to);
                out.transpose(delta);
            }
            let mut out: CompiledSong = out.into();
            out.joined = entry.joined;
            return Ok(out);
        }

        if let Some(song) = self.get_song(&entry.id_file) {
            let tonality = entry.tonic.unwrap_or(song.tonality);
            let delta = delta_tonality(song.tonality, tonality);
            let name = entry.rename.as_ref().unwrap_or(&song.name).to_string();

            let mut out = CompiledSong {
                name,
                bpm: song.bpm,
                tonality,
                joined: entry.joined,
                headless: false,
                sections: Vec::new(),
            };

            if let Some(named) = &entry.named_order {
                let order = song.orders.get(named);
                if let Some(index) = order {
                    let secs = index.iter().map(|i| song.sections[*i].clone());
                    out.sections.extend(secs);
                } else {
                    return Err(LSTLinkErrorKind::OrderNotFound(named.clone()));
                }
            } else if let Some(section_names) = &entry.explicit_order {
                for oelem in section_names {
                    match oelem{
                        OrderElement::Section(name)=>{
                            if let Some(i) = song.section_names.get(name) {
                                out.sections.push(song.sections[*i].clone());
                            } else {
                                return Err(LSTLinkErrorKind::SectionNotFound(name.clone()));
                            }
                        }
                        OrderElement::Annotation(annotation)=>{
                            out.sections.push(SongBlock::Annotation(annotation.clone()));
                        }
                    }
                }
            } else if let Some(default) = song
                .orders
                .get("default")
                .or_else(|| song.orders.get("main"))
            {
                for i in default {
                    out.sections.push(song.sections[*i].clone());
                }
            } else {
                out.sections = song
                    .sections
                    .iter()
                    .filter(|sblock| {
                        //TODO: we are ignoring annotations
                        if let SongBlock::Section(section) = sblock {
                            !section.name.ends_with('~')
                        } else {
                            false
                        }
                    })
                    .cloned()
                    .collect();
            }
            if delta != 0 {
                out.transpose(delta);
            }
            Ok(out)
        } else {
            Err(LSTLinkErrorKind::SongNotFound(entry.id_file.clone()))
        }
    }
    fn list_songs<'i>(&'i self) -> Box<dyn Iterator<Item = &'i CacheItem<Song>> + 'i>;
    fn compile_list(&self, list: &Songlist) -> (Vec<CompiledSong>, Vec<LSTLinkError>) {
        let mut out: Vec<CompiledSong> = Vec::new();
        let mut err_out: Vec<LSTLinkError> = Vec::new();
        let mut last_is_valid = false;
        for element in list {
            match element {
                SonglistElement::Entry(entry) => match self.compile_entry(entry) {
                    Ok(mut song) => {
                        if song.joined && last_is_valid {
                            out.last_mut().unwrap().sections.append(&mut song.sections);
                        } else {
                            out.push(song);
                        }
                        last_is_valid = true;
                    }
                    Err(kind) => {
                        err_out.push(LSTLinkError {
                            kind,
                            context: Some(entry.id_file.clone()),
                            line: entry.line,
                            loc: (0..0), //TODO
                        });
                        last_is_valid = false;
                    }
                },
                SonglistElement::Annotation(att) => {
                    out.push(CompiledSong {
                        bpm: None,
                        joined: false,
                        name: String::new(),
                        tonality: Tonality::default(),
                        headless: true,
                        sections: vec![SongBlock::Annotation(att.clone())],
                    });
                    continue;
                }
            };
        }
        (out, err_out)
    }
    fn load_songs(&mut self, data: impl IntoIterator<Item = CacheItem<Song>>);
    fn append_song(&mut self, data: CacheItem<Song>);
    fn remove_song(&mut self, handle: &str) -> Option<CacheItem<Song>>;
}

impl SongMemoryDatabase {
    pub fn new(cache: Vec<CacheItem<Song>>) -> Self {
        let mut index = HashMap::new();
        index.extend(
            cache
                .into_iter()
                .map(|cache_item| (cache_item.handle.clone(), cache_item)),
        );
        SongMemoryDatabase { data: index }
    }
    pub fn get_by_id(&self, id: &str) -> Option<&CacheItem<Song>> {
        self.data.get(id)
    }
}

impl SongDatabase for SongMemoryDatabase {
    fn get_song(&self, id: &str) -> Option<&Song> {
        self.data.get(id).map(|cache_item| &cache_item.data)
    }
    fn clear(&mut self) {
        self.data.clear();
    }
    fn list_songs<'i>(&'i self) -> Box<dyn Iterator<Item = &'i CacheItem<Song>> + 'i> {
        let a = self.data.iter().map(|(_, cache_item)| cache_item);
        Box::new(a)
    }
    fn load_songs(&mut self, data: impl IntoIterator<Item = CacheItem<Song>>) {
        self.data.clear();
        self.data.extend(
            data.into_iter()
                .map(|cache_item| (cache_item.handle.clone(), cache_item)),
        );
    }
    fn append_song(&mut self, data: CacheItem<Song>) {
        self.data.insert(data.handle.clone(), data);
    }
    fn remove_song(&mut self, handle: &str) -> Option<CacheItem<Song>> {
        self.data.remove(handle)
    }
}
