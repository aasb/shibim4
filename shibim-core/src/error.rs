use thiserror::Error;

use crate::base::{LSTParseError, LocatedError};

#[derive(Error,Debug)]
#[error("Error loading file '{file}'")]
pub struct FileError<T>{
    pub file : String,
    #[source]
    pub detail : T
}

//This is admittedly a bad, confusing design, i am sad
pub type LoadErrors<T> = FileError<ParseOrIOError<T>>;

#[derive(Error,Debug)]
pub enum LSTError{
    #[error(transparent)]
    LinkError(#[from] FileError<LSTLinkError>),
    #[error(transparent)]
    OpenError(#[from] LoadErrors<LSTParseError>)
}
pub type LSTLinkError = LocatedError<LSTLinkErrorKind>;
#[derive(Debug,Error)]

pub enum LSTLinkErrorKind{
    #[error("Song not found '{0}'")]
    SongNotFound(String),
    #[error("Section not found '{0}'")]
    SectionNotFound(String),
    #[error("Order not found '{0}'")]
    OrderNotFound(String),
}

#[derive(Error,Debug)]
#[error("Post-processing error, {msg}")]
pub struct VisitorError{
    pub msg : String,
    detail : Option<Box<dyn std::error::Error>>
}

#[derive(Error,Debug)]
pub enum ParseOrIOError<T>{
    #[error(transparent)]
    IOError(#[from] std::io::Error),
    #[error("Synax error(s) '{0:?}'")] //Todo, this is duct tape
    ParseError(Vec<T>)
}

