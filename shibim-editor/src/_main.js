import {EditorState} from "@codemirror/state";
import {EditorView, keymap} from "@codemirror/view";
import {defaultKeymap} from "@codemirror/commands";
import {basicSetup} from "codemirror";
/*

*/

let greetInputEl;
let greetMsgEl;

window.addEventListener("DOMContentLoaded", () => {
  greetInputEl = document.querySelector("#greet-input");
  greetMsgEl = document.querySelector("#greet-msg");
  document.getElementById("save-btn").addEventListener("click",()=>console.log("No"));
});

async function greet() {
  greetMsgEl.textContent = await invoke("greet", { name: greetInputEl.value });
}

window.greet = greet;

let startState = EditorState.create({
  doc: "Hello World",
  extensions: [basicSetup]
})

let view = new EditorView({
  state: startState,
  parent: document.getElementById("editor-area")
})