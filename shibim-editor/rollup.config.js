import { nodeResolve } from '@rollup/plugin-node-resolve';
import copy from 'rollup-plugin-copy'
import svelte from 'rollup-plugin-svelte';
import css from 'rollup-plugin-css-only';

export default [
{
    input : "src/main.js",
    output : {
        file : "build/bundle.js",
        format : "iife",
        name : "shb_app"
    },
    plugins: [
        copy({ 
            targets :[
                {src : ["../shibim-site/css/song.css","../shibim-site/css/fonts.css"],dest : "src"}
            ]
        }),
        svelte({
            compilerOptions : {
                dev : true
            }
        }),
        css({output : "bundle.css"}),
        nodeResolve(),
        copy({
            targets : [
                {src : ["src/**.html","src/**.css","src/assets"], dest : "build"},
            ]
        })
    ]
}
]