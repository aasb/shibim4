#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]
extern crate serde;
extern crate warp;
extern crate tokio;
extern crate tokio_stream;
extern crate futures_util;
extern crate shibim_core;
extern crate shibim_search;
use std::net::{SocketAddr,IpAddr,Ipv6Addr};
use std::path::Path;
use std::io::{Write,Read};
use std::fs::File;
use std::fmt::Display;
use std::collections::HashMap;
use futures_util::{StreamExt,SinkExt,join,TryStreamExt};
use tokio::sync::{
    mpsc::unbounded_channel,
    mpsc::UnboundedSender,
    RwLock as TRwLock
};
use  tokio_stream::wrappers::UnboundedReceiverStream;
use warp::{ws, Buf};
use shb::files::PathsRef;
use shb::SongDatabase;
use shibim_core as shb;
use shibim_search::SongIndexer;
use serde::{Deserialize, Serialize};
use std::sync::{
    Arc, RwLock, atomic::AtomicUsize, atomic::Ordering};
use tauri::Manager;

#[derive(Debug, Serialize, Deserialize)]
struct FileListJS {
    name: String,
    desc: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
enum UpdateEventJS<'i>{
    Global,
    SHB(&'i str),
    LST(&'i str),
    DelSHB(&'i str),
    DelLST(&'i str)
}

struct User{
    ws_conn : UnboundedSender<ws::Message>,
    sdp_text : Option<String>
  }
  
struct UserDB{
all : HashMap<usize, User>,
host : Option<usize>
}
type Users = Arc<TRwLock<UserDB>>;
impl Default for UserDB{
    fn default() -> Self{
        UserDB{
        all : HashMap::default(),
        host : None
        }
}
}
static NEXT_USER_ID: AtomicUsize = AtomicUsize::new(1);

//Todo: find how to get local ip without phoning the internet
pub fn get_ip() -> Option<IpAddr>{
    let addrs = [
        SocketAddr::from(([0, 0, 0, 0], 0)),
        SocketAddr::from(([127, 0, 0, 1], 35901)),
    ];
    std::net::UdpSocket::bind(&addrs[..])
        .and_then(|socket : std::net::UdpSocket|{
            socket.connect("1.1.1.1:80").or_else(|_|
                socket.connect("127.0.0.1:8081")
            )?;
            socket.local_addr()
        }).map(|x|x.ip()).ok()
}

  

#[derive(Debug, Serialize, Deserialize)]
struct SongListJS {
    name: String,
    desc: String,
    sections: Vec<FileListJS>,
}

#[derive(Debug, Serialize, Deserialize)]
struct SourceErrorJS {
    file: String,
    line: usize,
    context: Option<String>,
    text: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct FileErrorJS {
    file: String,
    message: String,
    exists: bool,
    was_opened: bool,
}

#[derive(Debug, Default)]
struct IndexNames {
    map: HashMap<String, usize>,
    last: usize,
}

type ListCache = HashMap<String, shb::files::CacheItem<Vec<shb::base::CompiledSong>>>;

fn lst_errors_to_js (errors : impl Iterator<Item=shb::error::LSTError>)->Vec<SourceErrorJS>{
    let mut js_errors = Vec::new();
    for lst_error in errors {
        match lst_error {
            shb::error::LSTError::LinkError(file_error) => {
                let lst_link_error = file_error.detail;
                js_errors.push(SourceErrorJS {
                    file: file_error.file,
                    line: lst_link_error.line,
                    context: lst_link_error.context,
                    text: lst_link_error.kind.to_string(),
                })
            }
            shb::error::LSTError::OpenError(load_error) => {
                let filename = load_error.file;
                match load_error.detail{
                    shb::error::ParseOrIOError::IOError(ioerr) => {
                        js_errors.push(SourceErrorJS {
                            file: filename,
                            line: 0,
                            context: None,
                            text: ioerr.to_string(),
                        });
                    }
                    shb::error::ParseOrIOError::ParseError(lst_parse_errs) => {
                        for parse_error in lst_parse_errs{
                            js_errors.push(SourceErrorJS {
                                file: filename.clone(),
                                line: parse_error.line,
                                context: parse_error.context,
                                text: parse_error.kind.to_string(),
                            });
                        }
                    }
                }
            }
        }
    }
    js_errors
}

fn shb_errors_to_js(errors : impl Iterator<Item=shb::error::LoadErrors<shb::base::SHBParseError>>) -> Vec<SourceErrorJS> {
    let mut js_errors: Vec<SourceErrorJS> = Vec::new();
    for file_error in errors {
        match file_error.detail {
            shb::error::ParseOrIOError::IOError(io_err) => {
                js_errors.push(SourceErrorJS {
                    file: file_error.file,
                    line: 0,
                    context: None,
                    text: io_err.to_string(),
                });
            }
            shb::error::ParseOrIOError::ParseError(parse_errors) => {
                for parse_error in parse_errors {
                    let content = parse_error.to_string();
                    js_errors.push(SourceErrorJS {
                        file: file_error.file.clone(),
                        line: 0,
                        context: parse_error.context,
                        text: content,
                    });
                }
            }
        }
    }
    js_errors
}

#[tauri::command]
fn greet(name: &str) -> String {
    format!("Hello, {}! You've been greeted from Rust!", name)
}



#[tauri::command]
fn load_shb_files(
    s_paths: tauri::State<shb::files::DefaultPathConf>,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
) -> Result<Vec<SourceErrorJS>, String> {
    let (cache, errors) = shb::files::get_dir_filelist_ext(
        (*s_paths).source_dir(shb::files::ResourceType::SHB),
        "shb",
    )
    .map(|paths| shb::files::load_batch_tolerant(paths, shb::files::try_read_shb, ""))
    .map_err(|err| err.to_string())?;
    s_cache
        .write()
        .map_err(|err| err.to_string())?
        .load_songs(cache);
    Ok(shb_errors_to_js(errors.into_iter()))
}

#[tauri::command]
fn copy_static_assets(s_paths: tauri::State<shb::files::DefaultPathConf>){
    (*s_paths).copy_static_assets().unwrap();
}

#[tauri::command]
fn load_lst_files(
    s_paths: tauri::State<shb::files::DefaultPathConf>,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
    list_cache: tauri::State<RwLock<ListCache>>,
) -> Result<Vec<SourceErrorJS>, String> {
    let resolver = (*s_cache).read().map_err(|err| err.to_string())?;

    let (lists, errors) = shb::files::get_dir_filelist_ext(
        (*s_paths).source_dir(shb::files::ResourceType::LST),
        "lst",
    )
    .map(|paths| shb::process_lst_batch(paths, "", &(*resolver),true))
    .map_err(|err| err.to_string())?;

    let mut list_cache = list_cache.write().map_err(|err| err.to_string())?;
    list_cache.clear();
    list_cache.extend(
        lists
            .into_iter()
            .map(|cache_item| (cache_item.handle.clone(), cache_item)),
    );
    Ok(lst_errors_to_js(errors.into_iter()))
}

#[tauri::command]
fn get_shb_list(cache: tauri::State<RwLock<shb::SongMemoryDatabase>>) -> Vec<FileListJS> {
    let cache = cache.read().unwrap();
    let mut out: Vec<_> = cache
        .list_songs()
        .map(|c_song| FileListJS {
            name: c_song.data.name.clone(),
            desc: c_song.handle.clone(),
        })
        .collect();
    out.sort_by(|ca, cb| ca.name.cmp(&cb.name));
    out
}

#[tauri::command]
fn get_lst_list(list_cache: tauri::State<RwLock<ListCache>>) -> Vec<FileListJS> {
    let cache = list_cache.read().unwrap();
    let mut out: Vec<_> = cache
        .values()
        .map(|list| FileListJS {
            name: list.handle.clone(),
            desc: String::new(),
        })
        .collect();
    out.sort_by(|ca, cb| cb.name.cmp(&ca.name));
    out
}

#[tauri::command]
fn rebuild_song_index(
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
    s_index: tauri::State<RwLock<shibim_search::MemoryTrieIndexer>>,
) {
    let cache = s_cache.read().unwrap();
    let mut index = s_index.write().unwrap();
    index.clear();
    for song in cache.list_songs() {
        index.index_song(&song.data, song.handle.clone());
    }
}
#[tauri::command]
fn get_song(
    id: &str,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>
) -> Option<SongListJS>{
    let cache = s_cache.read().unwrap();
    cache.get_song(id).map(|song| SongListJS{
        name : song.name.clone(),
        desc : "".to_owned(),
        sections: song.sections
        .iter()
        .filter_map(|sblock|sblock.section())
        .map(|sect| FileListJS {
            name: sect.name.clone(),
            desc: sect.description.clone(),
        })
        .collect() 
    })
}

#[tauri::command]
fn query_song(
    text: &str,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
    s_index: tauri::State<RwLock<shibim_search::MemoryTrieIndexer>>,
) -> Vec<SongListJS> {
    let cache = s_cache.read().unwrap();
    let index = s_index.read().unwrap();
    index
        .query_text(text)
        .into_iter()
        .map(|x| SongListJS {
            name: cache.get_by_id(x).unwrap().data.name.clone(),
            desc: x.to_owned(),
            sections: cache
                .get_by_id(x)
                .unwrap()
                .data
                .sections
                .iter()
                .filter_map(|sblock|sblock.section())
                .map(|sect| FileListJS {
                    name: sect.name.clone(),
                    desc: sect.description.clone(),
                })
                .collect(),
        })
        .collect()
}

#[tauri::command]
fn get_text_file(
    handle: &str,
    kind: &str,
    s_paths: tauri::State<shb::files::DefaultPathConf>,
) -> Result<String, String> {
    let r_kind = match kind {
        "shb" => shb::files::ResourceType::SHB,
        "lst" => shb::files::ResourceType::LST,
        _ => return Err("Unreconized resource type".to_string()),
    };

    let fname = std::path::PathBuf::from(handle);
    let fname = fname
        .file_name()
        .ok_or("Not a valid handle name :".to_string() + handle)?;
    if fname != handle {
        return Err("Not a valid handle name (contains directories)".to_string());
    }
    let mut fname = std::path::PathBuf::from(fname);
    fname.set_extension(kind);
    std::fs::read_to_string(s_paths.source(&fname, r_kind)).map_err(|err| err.to_string())
}

#[tauri::command]
fn compile_shb_string(
    text: &str,
    handle: &str,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
    app_handle: tauri::AppHandle,
) -> Result<Vec<SourceErrorJS>, String> {
    let (song, errors) = shb::parser::parse_shb(text);
    s_cache
        .write()
        .map_err(|e| e.to_string())?
        .append_song(shb::files::CacheItem {
            file_name: std::path::PathBuf::from(handle.to_owned() + ".shb"),
            modified: None, //TODO
            handle: handle.to_owned(),
            data: song,
        });
    //Hmm... do we return on event error? The cache was updated anways.
    app_handle.emit_all("cache_updated",UpdateEventJS::SHB(&handle)).map_err(|e|e.to_string())?;
    Ok(errors
        .into_iter()
        .map(|err| SourceErrorJS {
            text: err.kind.to_string(),
            file: handle.to_owned(),
            line: err.line,
            context: err.context,
        })
        .collect())
}

#[tauri::command]
fn try_compile_shb_string(
    text: &str,
    handle: &str,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
    app_handle: tauri::AppHandle,
) -> Result<Vec<SourceErrorJS>, String> {
    match s_cache.try_write() {
        Ok(mut cache) => {
            let (song, errors) = shb::parser::parse_shb(text);
            cache.append_song(shb::files::CacheItem {
                file_name: std::path::PathBuf::from(handle.to_owned() + ".shb"),
                modified: None, //TODO
                handle: handle.to_owned(),
                data: song,
            });
            app_handle.emit_all("cache_updated",UpdateEventJS::SHB(&handle));
            Ok(errors
                .into_iter()
                .map(|err| SourceErrorJS {
                    text: err.kind.to_string(),
                    file: handle.to_owned(),
                    line: err.line,
                    context: err.context,
                })
                .collect())
        }
        Err(e) => Err(e.to_string()),
    }
}

#[tauri::command]
fn compile_lst_string(
    text: &str,
    handle: &str,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
    s_list_cache: tauri::State<RwLock<ListCache>>,
    app_handle: tauri::AppHandle,
) {
    let song_cache = s_cache.read().unwrap();
    let (list, syn_errors) = shb::parser::parse_lst(text);
    let (list, link_errors) = song_cache.compile_list(&list);
    let mut list_cache = s_list_cache.write().unwrap();
    list_cache.insert(
        handle.to_owned(),
        shb::files::CacheItem {
            file_name: std::path::PathBuf::from(handle.to_owned() + ".lst"),
            handle: handle.to_owned(),
            modified: None,
            data: list,
        },
    );
    app_handle.emit_all("cache_updated",UpdateEventJS::LST(&handle));
}

#[tauri::command]
fn try_compile_lst_string(
    text: &str,
    handle: &str,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
    s_list_cache: tauri::State<RwLock<ListCache>>,
    app_handle: tauri::AppHandle,
) -> Result<Vec<SourceErrorJS>, String> {
    if let Ok(song_cache) = s_cache.try_read() {
        if let Ok(mut list_cache) = s_list_cache.try_write() {
            let (list, syn_errors) = shb::parser::parse_lst(text);
            let (list, link_errors) = song_cache.compile_list(&list);
            list_cache.insert(
                handle.to_owned(),
                shb::files::CacheItem {
                    file_name: std::path::PathBuf::from(handle.to_owned() + ".lst"),
                    handle: handle.to_owned(),
                    modified: None,
                    data: list,
                },
            );
            let mut js_errors = Vec::new();
            for err in syn_errors{
                js_errors.push(SourceErrorJS{
                    file : handle.to_owned(),
                    line : err.line,
                    text : err.kind.to_string(),
                    context : err.context
                });
            }
            for err in link_errors{
                js_errors.push(SourceErrorJS{
                    file : handle.to_owned(),
                    line : err.line,
                    text : err.kind.to_string(),
                    context : err.context
                });
            }
            app_handle.emit_all("cache_updated",UpdateEventJS::LST(&handle));
            return Ok(js_errors);
        }
    }
    Err("Cache busy".to_string())
}
#[tauri::command]
fn get_html_cache_lst(
    handle: &str,
    s_list_cache: tauri::State<RwLock<ListCache>>,
) -> Result<String, String> {
    let list_cache = s_list_cache.read().map_err(|e| e.to_string())?;
    let list = &list_cache
        .get(handle)
        .ok_or("List not found in cache")?
        .data;
    Ok(shb::html::Songlist { list: &list }.to_string())
}

#[tauri::command]
fn get_html_cache_shb(
    handle: &str,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
) -> Result<String, String> {
    let cache = s_cache.read().map_err(|e| e.to_string())?;
    let song = cache.get_song(handle).ok_or("Song not found in cache")?;
    Ok(shb::html::Song { song }.to_string())
}

#[tauri::command]
fn get_html_string_shb(text: &str) -> Result<String, String> {
    let (song, errors) = shb::parser::parse_shb(text);
    Ok(shb::html::Song { song: &song }.to_string())
}

#[tauri::command]
fn close_window(window: tauri::Window) {
    window.close();
}
/*
#[tauri::command]
async fn dialog_confirm_close(
    file : String,
    kind : String,
    handle: tauri::AppHandle
){
    let script = format!("file_name = \"{}\"; kind = \"{}\"",file,kind);
    let docs_window = tauri::WindowBuilder::new(
        &handle,
        "dialog_confirm_close",
        tauri::WindowUrl::App("dialog_confirm_close".into())
      )
      .skip_taskbar(true)
      .inner_size(400.0,150.0)
      .center()
      .decorations(false)
      .initialization_script(&script).build().unwrap();
      //TODO: Sanitize this or find a cleaner way to share filename
}*/
#[tauri::command]
fn write_cache(
    s_paths: tauri::State<shb::files::DefaultPathConf>,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
    s_list_cache: tauri::State<RwLock<ListCache>>
){
    let cache = s_cache.read().unwrap();
    let mut errors = Vec::new();
    let mut f_options = std::fs::File::options();
    f_options.write(true)
        .create(true)
        .truncate(true);
    for cache_item in cache.list_songs(){
        let mut path = s_paths.output(&cache_item.handle, shb::files::ResourceType::SHB);
        path.set_extension("html");
        match f_options.open(&path){
            Ok(mut file) =>{
                let el = shb::html::SongPage{song : &cache_item.data, prov : &(*s_paths), static_js : false, lang: shb::i18n::Lang::ES };
                if let Err(err) = write!(file,"{}",el){
                    errors.push(FileErrorJS{
                        file : path.to_string_lossy().to_string(),
                        was_opened : true,
                        exists : true,
                        message : err.to_string()
                    });    
                }
            },
            Err(err) =>{
                errors.push(FileErrorJS{
                    file : path.to_string_lossy().to_string(),
                    message: err.to_string(),
                    was_opened : false,
                    exists : path.exists()
                });
            }
        }
    }
    if let Err(err) = write_song_index(&*cache,&*s_paths){
        errors.push(FileErrorJS{
                file : "[Song Index]".to_string(),
                message: err.to_string(),
                was_opened : false,
                exists : true
            });
    }
    let list_cache = s_list_cache.read().unwrap();
    for (_handle, cache_item) in list_cache.iter(){
        let mut path = s_paths.output(&cache_item.handle, shb::files::ResourceType::LST);
        path.set_extension("html");
        match f_options.open(&path){
            Ok(mut file) =>{
                let el = shb::html::SonglistPage{list : &cache_item.data, prov : &(*s_paths), static_js : false, lang: shb::i18n::Lang::ES };
                if let Err(err) = write!(file,"{}",el){
                    errors.push(FileErrorJS{
                        file : path.to_string_lossy().to_string(),
                        was_opened : true,
                        exists : true,
                        message : err.to_string()
                    });    
                }
            },
            Err(err) =>{
                errors.push(FileErrorJS{
                    file : path.to_string_lossy().to_string(),
                    message: err.to_string(),
                    was_opened : false,
                    exists : path.exists()
                });
            }
        }
    }
    if let Err(err) = write_list_index(&*list_cache,&*s_paths){
        errors.push(FileErrorJS{
            file : "[List Index]".to_string(),
            message: err.to_string(),
            was_opened : false,
            exists : true
        });
    }
}
#[tauri::command]
fn write_text_file(
    text: &str,
    handle: &str,
    kind: &str,
    force: bool,
    s_paths: tauri::State<shb::files::DefaultPathConf>,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
    s_list_cache: tauri::State<RwLock<ListCache>>,
    s_index: tauri::State<RwLock<shibim_search::MemoryTrieIndexer>>,
    app_handle: tauri::AppHandle
) -> Result<(), FileErrorJS> {
    let rtype = match kind {
        "shb" => shb::files::ResourceType::SHB,
        "lst" => shb::files::ResourceType::LST,
        _ => {
            return Err(FileErrorJS {
                message: "Unrecognized file type".to_string(),
                file: handle.to_owned(),
                exists: false,
                was_opened: false,
            })
        }
    };

    let mut path = s_paths.source(handle, rtype);
    path.set_extension(kind);
    let mut file = std::fs::File::options()
        .write(true)
        .create_new(!force)
        .create(force)
        .truncate(force)
        .open(&path)
        .map_err(|x| FileErrorJS {
            file: handle.to_owned(),
            message: x.to_string(),
            exists: path.exists(),
            was_opened: false,
        })?;
    file.write_all(text.as_bytes()).map_err(|x| FileErrorJS {
        file: handle.to_owned(),
        message: x.to_string(),
        exists: true,
        was_opened: true,
    })?;
    //Todo: error handling
    let html = match kind {
        "shb" => {
            compile_shb_string(text, handle, s_cache.clone(), app_handle);
            let song_cache = s_cache.read().unwrap();
            let song = song_cache.get_song(handle).unwrap();
            let mut index = s_index.write().unwrap();
            index.remove_song(handle);
            index.index_song(song,handle);
            shb::html::SongPage {
                song: song,
                prov: s_paths.inner(),
                static_js: false,
                lang: shb::i18n::Lang::ES,
            }
            .to_string()
        }
        "lst" => {
            compile_lst_string(text, handle, s_cache.clone(), s_list_cache.clone(),app_handle);
            let list_cache = s_list_cache.read().unwrap();
            let list = &list_cache.get(handle).unwrap().data;
            shb::html::SonglistPage {
                list: list,
                prov: s_paths.inner(),
                static_js: false,
                lang: shb::i18n::Lang::ES,
            }
            .to_string()
        }
        _ => {
            unreachable!()
        }
    };
    let mut path = s_paths.output(handle, rtype);
    path.set_extension("html");
    let mut file = std::fs::File::create(&path).map_err(|err| FileErrorJS {
        message: err.to_string(),
        file: handle.to_owned(),
        exists: path.exists(),
        was_opened: false,
    })?;
    file.write_all(html.as_bytes()).map_err(|x| FileErrorJS {
        file: handle.to_owned(),
        message: x.to_string(),
        exists: true,
        was_opened: true,
    })?;
    //Update html index
    match kind {
        "shb" => {
            let song_cache = s_cache.read().unwrap();
            write_song_index(&*song_cache,&*s_paths).unwrap();
            /*
            let mut sorted = song_cache
                .list_songs()
                .map(|cache_item| shb::files::song_cache_to_index(cache_item, &*s_paths))
                .collect::<Vec<shb::base::SongIndexEntry>>();
            sorted.sort_by(|a, b| a.name.cmp(&b.name));
            let index = shb::html::SongIndex { list: sorted };
            std::fs::write(
                s_paths.output("songs.html", shb::files::ResourceType::STATIC),
                index.to_string(),
            )
            .unwrap();*/
        }
        "lst" => {
            let list_cache = s_list_cache.read().unwrap();
            write_list_index(&*list_cache,&*s_paths).unwrap();
            /*
            let mut sorted = list_cache
                .values()
                .map(|cache_item| shb::files::list_cache_to_index(cache_item, &*s_paths))
                .collect::<Vec<shb::base::SonglistIndexEntry>>();
            sorted.sort_by(|a, b| b.name.cmp(&a.name));
            let index = shb::html::SonglistIndex { list: sorted };
            std::fs::write(
                s_paths.output("songlists.html", shb::files::ResourceType::STATIC),
                index.to_string(),
            )
            .unwrap();
            */
        }
        _ => {}
    }
    Ok(())
}
fn write_song_index(song_cache : &impl shb::SongDatabase, s_paths : &impl shb::files::PathsRef)->std::io::Result<()>{
    let mut sorted = song_cache
        .list_songs()
        .map(|cache_item| shb::files::song_cache_to_index(cache_item, &*s_paths))
        .collect::<Vec<shb::base::SongIndexEntry>>();
    sorted.sort_by(|a, b| a.name.cmp(&b.name));
    let index = shb::html::SongIndex { list: sorted };
    let mut file = std::fs::File::create(s_paths.output("songs.html", shb::files::ResourceType::STATIC))?;
    write!(file,"{}",index)?;
    let search_page = shb::html::SearchPage{prov : s_paths, lang: shb::i18n::Lang::ES};
    let mut search_file = std::fs::File::create(s_paths.output("search.html", shb::files::ResourceType::STATIC))?;
    write!(search_file,"{}",search_page)?;
    write_json_text(
        s_paths.output("text.js", shb::files::ResourceType::STATIC),
        &*s_paths,
        song_cache.list_songs(),
    )
}
fn write_list_index(list_cache : &ListCache, s_paths : &impl shb::files::PathsRef)->std::io::Result<()>{
    let mut sorted = list_cache.values()
        .map(|cache_item| shb::files::list_cache_to_index(cache_item, &*s_paths))
        .collect::<Vec<shb::base::SonglistIndexEntry>>();
    sorted.sort_by(|a, b| b.name.cmp(&a.name));
    let index = shb::html::SonglistIndex { list: sorted };
    let mut file = std::fs::File::create(s_paths.output("songlists.html", shb::files::ResourceType::STATIC))?;
    write!(file,"{}",index)
}
#[tauri::command]
fn delete_text_file(
    handle: &str,
    kind: &str,
    s_paths: tauri::State<shb::files::DefaultPathConf>,
    s_cache: tauri::State<RwLock<shb::SongMemoryDatabase>>,
    s_index: tauri::State<RwLock<shibim_search::MemoryTrieIndexer>>,
    s_list_cache: tauri::State<RwLock<ListCache>>,
    app_handle: tauri::AppHandle,
) -> Result<(), FileErrorJS> {
    let rtype = match kind {
        "shb" => shb::files::ResourceType::SHB,
        "lst" => shb::files::ResourceType::LST,
        _ => {
            return Err(FileErrorJS {
                message: "Unrecognized file type".to_string(),
                file: handle.to_owned(),
                exists: false,
                was_opened: false,
            })
        }
    };
    let mut path = s_paths.source(handle, rtype);
    path.set_extension(kind);
    let res = std::fs::remove_file(&path).map_err(|x| FileErrorJS {
        message: x.to_string(),
        file: handle.to_owned(),
        exists: path.exists(),
        was_opened: false,
    });
    if res.is_ok() {
        match kind {
            "shb" => {
                let mut cache = s_cache.write().unwrap();
                cache.remove_song(handle);
                let mut index = s_index.write().unwrap();
                index.remove_song(handle);
                app_handle.emit_all("cache_updated",UpdateEventJS::DelSHB(&handle));
            }
            "lst" => {
                let mut cache = s_list_cache.write().unwrap();
                cache.remove(handle);
                app_handle.emit_all("cache_updated",UpdateEventJS::DelLST(&handle));
            }
            _ => unreachable!(),
        }
    };
    res
}
#[tauri::command]
fn c_get_ip() -> (String,String){
    let ip = get_ip().unwrap_or("127.0.0.1".parse().unwrap());
    let soc = SocketAddr::from((ip,2020));
    let soc2 = SocketAddr::from((ip,2021));
    (soc.to_string(),soc2.to_string())
}
fn try_read_path_arg() -> Option<std::path::PathBuf>{
    let arg_string = std::env::args().nth(1)?;
    Path::new(&arg_string).canonicalize().ok()
}
fn try_read_path_env() -> Option<std::path::PathBuf>{
    let path_var = std::env::var("SHIBIM_PATH").ok();
    let appimg_var = 
        if cfg!(unix) {std::env::var("APPIMAGE").ok()}
        else {None};
    path_var
        .map(std::path::PathBuf::from)
        .and_then(|x| x.canonicalize().ok())
        .or(
            appimg_var
            .map(std::path::PathBuf::from)
            .and_then(
                |x| x.parent().map(|x|x.to_owned())
            )
        )
}
fn try_read_path_exe() -> Option<std::path::PathBuf>{
    std::env::current_exe().ok().and_then(
        |x| x.parent().map(|x|x.to_owned())
    )
}
fn try_read_path_cwd() -> Option<std::path::PathBuf>{
    std::env::current_dir().ok()
}
fn try_read_path_all() -> Option<std::path::PathBuf>{
    if let Some(path) = try_read_path_arg(){
        Some(path)
    }else
    if let Some(path) = try_read_path_env(){
        Some(path)
    }else
    if let Some(path) = try_read_path_exe(){
        Some(path)
    }else
    if let Some(path) = try_read_path_cwd(){
        Some(path)
    }else{
        None
    }
}
fn main() {
    let base_path = try_read_path_all().unwrap();
    
    println!("Base directory {}", base_path.display());
    let mut paths = shb::files::DefaultPathConf::new(
        base_path.join("dat/lst/"),
        base_path.join("dat/shb/"),
        base_path.join("shibim-site"),
        base_path.join("web/"),
    );
    paths.static_preload("js/song.js").unwrap();
    paths.static_preload("css/song.css").unwrap();
    paths.static_preload("css/song_page.css").unwrap();
    paths.static_preload("css/fonts.css").unwrap();

    paths.make_output_dirs();
    paths.make_input_dirs();

    let song_cache = RwLock::new(shb::SongMemoryDatabase::default());
    let list_cache: RwLock<ListCache> = RwLock::new(HashMap::new());
    let song_index = RwLock::new(shibim_search::MemoryTrieIndexer::new(shb::i18n::Lang::ES));
    let users = Users::default();
    tauri::async_runtime::spawn(server_http(base_path.join("web/"),base_path.join("cert/localhost.crt"),base_path.join("cert/localhost.key"),users.clone(),true));

    tauri::Builder::default()
        .setup(|app| {
            let main_window = app.get_window("main").unwrap();
            let handle = app.handle().clone();
            app.listen_global("app_mount", move |event| {
                println!("Loading sources...");
                load_shb_files(
                    handle.state::<shb::files::DefaultPathConf>(),
                    handle.state::<RwLock<shb::SongMemoryDatabase>>(),
                )
                .unwrap();
                load_lst_files(
                    handle.state::<shb::files::DefaultPathConf>(),
                    handle.state::<RwLock<shb::SongMemoryDatabase>>(),
                    handle.state::<RwLock<ListCache>>(),
                )
                .unwrap();
                println!("Done loading sources");
                handle.emit_all("cache_updated", UpdateEventJS::Global).unwrap();
            });
            Ok(())
        })
        .manage(paths)
        .manage(song_cache)
        .manage(list_cache)
        .manage(song_index)
        .invoke_handler(tauri::generate_handler![
            load_shb_files,
            load_lst_files,
            get_shb_list,
            get_lst_list,
            get_text_file,
            query_song,
            rebuild_song_index,
            try_compile_shb_string,
            compile_shb_string,
            try_compile_lst_string,
            compile_lst_string,
            get_html_cache_shb,
            get_html_cache_lst,
            get_html_string_shb,
            write_text_file,
            close_window,
            delete_text_file,
            copy_static_assets,
            c_get_ip,
            write_cache,
            get_song,
            greet
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

fn write_json_text<'i>(
    path: impl AsRef<Path>,
    hpaths: &impl PathsRef,
    cache_items: impl Iterator<Item = &'i shb::files::CacheItem<shb::base::Song>>,
) -> Result<(), std::io::Error> {
    let mut file = File::create(path)?;
    write!(file, "let _text_js = [")?;
    for (i, cache_song) in cache_items.enumerate() {
        if i > 0 {
            writeln!(file, ",").unwrap();
        }
        write_json_entry(
            &mut file,
            i,
            &cache_song.data.name,
            &cache_song.handle,
            hpaths.rel_out(
                cache_song.handle.clone() + ".html",
                shb::files::ResourceType::SHB,
                shb::files::ResourceType::STATIC,
            ),
            cache_song.data.norm_text(shb::i18n::Lang::ES),
        );
    }
    write!(file, "]").unwrap();
    Ok(())
}

fn write_json_entry(
    file: &mut impl Write,
    id: usize,
    name: impl Display,
    handle: impl Display,
    href: impl Display,
    content: impl Display,
) {
    write!(
        file,
        "{{id:{}, name:\"{}\", handle:\"{}\", href:\"{}\", text:\"{}\"}}",
        id, name, handle, href, content
    )
    .expect("Error writing text extract");
}


//Server functions


async fn upload(form : warp::multipart::FormData, mut home : std::path::PathBuf) -> Result<impl warp::Reply, warp::Rejection>{
    let mut f_buff = Vec::new();
    let fragments : Vec<warp::multipart::Part> =
        form.try_collect().await.map_err(|e|{
            eprintln!("Upload error (mutipart form): {}",e);
            warp::reject::reject()
        })?;
    for frag in fragments{
        if frag.name() == "file"{
            //TODO: PLEASE CHECK THIS; might have SEVERE consecuences to get this worng.
            let name = frag.filename().unwrap_or("pdf.pdf");
            let name = Path::new(name).file_name().unwrap();
            home.push(name);
            println!("{:#?}",&home);
            let content : Vec<_> = frag.stream().try_fold(Vec::new(),|mut vec, data|{
                vec.push(data);
                async move { Ok(vec) }
            })
            .await
            .map_err(|err|{
                eprintln!("Upload error (file content) : {}",err);
                warp::reject::reject()
            })?;
            let mut f = File::create(&home).map_err(|err|{
                eprintln!("Upload error (fs error) : {}",err);
                warp::reject::reject()
            })?;

            for bytes in content{
                bytes.reader().read_to_end(&mut f_buff);
            }

            f.write_all(&f_buff).map_err(|err|{
                eprintln!("Upload error (write error) : {}",err);
                warp::reject::reject()
            })?;
        }
    }
    Ok("OK")
}
async fn server_http(web_path : std::path::PathBuf, cert_path : impl AsRef<Path>, key_path : impl AsRef
    <Path>, users:Users, dup_http : bool){
    use crate::warp::Filter;
    //todo: gracefully shutdown
    let web_path_upload = web_path.join("upload");
    if !web_path_upload.exists(){
        if let Err(e) = std::fs::create_dir_all(&web_path_upload){
            eprintln!("Error creating upload dir: {}",e);
        };
    }
    let default = web_path.join("songs.html");
    let default_filter = warp::path::end()
      .and(warp::fs::file(default));
    let serve_filter = warp::get()
    .and(warp::fs::dir(web_path.clone()));
    //Enable websockets when using this local server
    let present_redirect = warp::path("query")
      .map(||{"true"});
    let upload_filter = warp::path("upload")
        .and(warp::post())
        .and(warp::multipart::form().max_length(64_000_000))
        .and_then(move |form|{
            upload(form, web_path_upload.clone())
        });
    //todo refactor and improve this me)ss
    //adapted from websockets_chat example from warp
    let users = warp::any().map(move || users.clone());
    let websockets = warp::path("ws")
      .and(warp::ws())
      .and(users).map(|ws: warp::ws::Ws, users : Users| {
        ws.on_upgrade(move |socket| user_connected(socket, users))
    });
    let resolver = websockets.or(default_filter.or(present_redirect.or(serve_filter.or(upload_filter))));
    let (addr,addr_tls) = if cfg!(windows) {
        (
            SocketAddr::from(([0,0,0,0],2020)),
            SocketAddr::from(([0,0,0,0],2021))
        )
    }else{
        (
            SocketAddr::from(([0,0,0,0,0,0,0,0],2020)),
            SocketAddr::from(([0,0,0,0,0,0,0,0],2021))
        )
    };
    if !dup_http {
        warp::serve(resolver).tls().cert_path(cert_path).key_path(key_path)
        .run(addr).await;
    }else{
        join!(
            warp::serve(resolver.clone()).tls().cert_path(cert_path).key_path(key_path)
                .run(addr_tls),
            warp::serve(resolver).run(addr)
        );
    }
  }

async fn user_connected(ws: warp::ws::WebSocket, users: Users) {
    // Use a counter to assign a new unique ID for this user.
    let my_id = NEXT_USER_ID.fetch_add(1, Ordering::Relaxed);
  
    eprintln!("New connection #{}", my_id);
  
    // Split the socket into a sender and receive of messages.
    let (mut user_ws_tx, mut user_ws_rx) = ws.split();
  
    // Use an unbounded channel to handle buffering and flushing of messages
    // to the websocket...
    let (tx, rx) = unbounded_channel();
    let mut rx = UnboundedReceiverStream::new(rx);
    tx.send(warp::ws::Message::text(format!("{{\"type\":\"UID\",\"data\":{} }}",my_id)));
    tokio::task::spawn(async move {
        while let Some(message) = rx.next().await {
            user_ws_tx
                .send(message)
                .await.unwrap_or_else(|e| {
                    eprintln!("websocket send error: {}", e);
                });
        }
    });
    
    // Save the sender in our list of connected users.
    users.write().await.all.insert(my_id, User{ws_conn:tx,sdp_text:None});
  
    // Return a `Future` that is basically a state machine managing
    // this specific user's connection.
  
    // Every time the user sends a message, broadcast it to
    // all other users...
    while let Some(result) = user_ws_rx.next().await {
        let msg = match result {
            Ok(msg) => msg,
            Err(e) => {
                eprintln!("websocket error(uid={}): {}", my_id, e);
                break;
            }
        };
        user_message(my_id, msg, &users).await;
    // user_ws_rx stream will keep processing as long as the user stays
    // connected. Once they disconnect, then...
    user_disconnected(my_id, &users).await;
  }
}


async fn user_message(my_id: usize, msg: warp::ws::Message, users: &Users) {
    // Skip any non-Text messages...
    let msg = if let Ok(s) = msg.to_str() {
        s
    } else {
        return;
    };
    let new_msg = format!("{}", msg);
    println!("Boradcast message : [{}]",new_msg);
    // New message from this user, send it to everyone else (except same uid)...
    for (&uid, usr) in users.read().await.all.iter() {
        if my_id != uid {
            if let Err(_disconnected) = usr.ws_conn.send(warp::ws::Message::text(new_msg.clone())) {
                // The tx is disconnected, our `user_disconnected` code
                // should be happening in another task, nothing more to
                // do here.
            }
        }
    }
  }
  
  

async fn user_disconnected(my_id: usize, users: &Users) {
    // Stream closed up, so remove from the user list
    let mut db_lock = users.write().await;
    db_lock.all.remove(&my_id);
    if db_lock.host == Some(my_id){
      db_lock.host = None;
    }
  }

  