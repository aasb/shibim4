let conf_enabled = false;
        function createSpinner() {
            let spinner = document.createElement("div");
            spinner.className = "lds-spinner";
            spinner.appendChild(document.createElement("div"));
            spinner.appendChild(document.createElement("div"));
            spinner.appendChild(document.createElement("div"));
            return spinner;
        }

        if (document.readyState === "interactive" || document.readyState === "complete"){
            load_search_js();
        }else{
            window.addEventListener('DOMContentLoaded',load_search_js)
        }

        function load_search_js () {
            let miniSearch = new MiniSearch({
                fields: ['name', 'text'], // fields to index for full-text search
                storeFields: ['name'] // fields to return with search results
            });
            let options = {
                prefix: true,
                boost: { name: 1.6 },
                combineWith: 'AND'
            }
            document.getElementById("search-text").addEventListener("input", (evt) => {
                let str = document.getElementById("search-text").value
                if (str.length > 0) {
                    results = miniSearch.search(str, options)
                    update_search(results)
                } else {
                    document.getElementById("search-results").innerHTML = "";
                }

            });

            miniSearch.addAll(_text_js);

            function update_search(data, max_res = 18) {
                let div = document.getElementById("search-results");
                div.innerHTML = ""
                let ul = document.createElement("UL");
                ul.className = "u-cardview";
                for (let i = 0; i < data.length && i < max_res; i++) {
                    const element = data[i];

                    entry = document.createElement("LI");
                    span = document.createElement("SPAN");
                    text = document.createElement("SPAN");
                    incl = document.createElement("SPAN");
                    link = document.createElement("A");
                    rname = document.createElement("SPAN");
                    link.className = "u-rname"
                    incl.className = "add";
                    incl.unlocked = true;
                    span.className = "s-result";
                    (function (incl) {
                        incl.addEventListener("click", (evt) => {
                            if (incl.unlocked) {
                                incl.unlocked = false
                                spin = createSpinner();
                                incl.parentElement.insertBefore(spin, incl.nextSibling);
                                fetch_song(_text_js[data[i].id].href, () => {
                                    incl.parentElement.removeChild(spin)
                                    incl.unlocked = true;
                                });
                            }
                        });
                    })(incl);
                    incl.textContent = "(añadir)";
                    text.textContent = _text_js[data[i].id].text.slice(0, 180);
                    rname.textContent = _text_js[data[i].id].handle;
                    rname.className = "loc";
                    text.className = "text"
                    link.textContent = data[i].name;
                    link.setAttribute("href", _text_js[data[i].id].href);
                    span.appendChild(rname);
                    span.appendChild(incl);
                    span.appendChild(link);
                    span.appendChild(text);
                    entry.appendChild(span);
                    ul.appendChild(entry);
                }
                div.appendChild(ul);
            }
            function fetch_song(url, cb) {
                var data = fetch(url)
                    .then((response) => response.text())
                    .then((text) => {
                        let parser = new DOMParser();
                        let doc = parser.parseFromString(text, "text/html");
                        let el = doc.getElementsByClassName("u-song")[0];
                        let target = document.getElementsByClassName("u-container")[0];
                        if (!conf_enabled) {
                            conf_enabled = true;
                            window.onbeforeunload = confirm;
                        }
                        target.appendChild(el);
                        setLocalButtons(el);
                        setPresentationClick(el);
                        cb();
                    });
            }
            function confirm() {
                return 'La lista guardada se perderá, continuar?';
            }
        }