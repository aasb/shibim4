use crate::result::*;
use PartialResult::*;
/*
pub enum NoteModifier {
    Accent,
    Normal,
    Ghost,
}
*/
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum NoteName {
    C = 0,
    D = 1,
    E = 2,
    F = 3,
    G = 4,
    A = 5,
    B = 7,
}


#[derive(Debug, PartialEq, Clone, Eq, Copy)]
pub enum ARestKind {
    RestMeasure(u16),
    RestRelative((u16,u16))
}
pub use ARestKind::*;

#[derive(Debug, PartialEq, Clone, Eq, Copy)]
pub struct  ARest {
    visible : bool,
    kind : ARestKind
}


#[derive(Debug, PartialEq, Clone, Eq)]
pub struct NoteHeight {
    name: NoteName,
    octave: i8,
    alt: i8,
}

#[derive(Debug, PartialEq, Clone, Eq)]
pub struct ANote {
    height: NoteHeight,
    duration: (u16, u16),
    tie: bool,
}

#[derive(Debug, PartialEq, Clone, Eq, Copy)]
pub enum ABCErrorType {
    UnexpectedChar(char),
    ExpectedNote,
    ExpectedTime,
    ExpectedRest,
    InvalidTime,
    NumberOverflow,
    InvalidNoteSyntax,
    ModifierOverflow,
}
#[derive(Debug, PartialEq, Clone, Eq)]
pub struct ABCParseError {
    kind: ABCErrorType,
    offset: (usize, usize),
}

impl ABCParseError {
    pub fn add_offset(&mut self, offset: usize) {
        self.offset.0 += offset;
        self.offset.1 += offset;
    }
}
pub fn parse_note_elem(s : &str){
    
}
pub fn parse_note_group(s : &str){
    let mut ns = s;
    loop{
        let lh = ns.chars().next();
        if matches!(lh, Some( ':'| ':' | '|' ) | None){

        }
    }
}

pub fn parse_rest(s : &str)-> (PartialResult<ARest,ABCParseError>,&str){
    let mut ns = s;
    let mut out = ARest{
        visible : true,
        kind : RestMeasure(1)
    };
    match s.chars().next(){
        Some(c @ ('z' | 'x'))=>{
            ns = &s[1..];
            out.visible = c == 'z';
            match parse_note_length(ns) {
           (Success(f),xs) => {
                out.kind = RestRelative(f);
                ns = xs;
            }
            (PartialError { partial,mut err },xs)=>{
                out.kind = RestRelative(partial);
                err.add_offset(1);
                return (PartialError { partial: out, err },xs);
            }
             _ => {
                out.kind = RestRelative((1,1));
            }
            }
        }
        Some(c @ ('Z'|'X'))=>{
            ns = &s[1..];
            out.visible = c == 'Z';
            match parse_dec(ns) {
                (Success(n),xs) => {
                    if let Some(n) = TryInto::<u16>::try_into(n).ok() {
                        out.kind = RestMeasure(n);
                    }else{
                        out.kind = RestMeasure(u16::MAX);
                        return (PartialError { partial: out, err: ABCParseError{
                            kind : ABCErrorType::NumberOverflow,
                            offset : (1, s.len()-xs.len())
                        } },xs);
                    }
                    ns = xs;
                }
                (FullError(_),_xs) => {
                    out.kind = RestMeasure(1);
                }
                (PartialError { partial, mut err },xs)=>{
                    err.add_offset(1);
                    out.kind = RestMeasure(partial.try_into().unwrap_or_default());
                    return (PartialError { partial: out, err },xs);
                }
            }
        }
        _=>{
            return (FullError(ABCParseError{
                kind : ABCErrorType::ExpectedRest,
                offset : (0,1)
            }),s);
        }
    };
    (Success(out),ns)
}

pub fn parse_dec(s : &str) -> (PartialResult<i64,ABCParseError>,&str) {
    let mut ns = s;
    let mut val: Option<i64> = Some(0);
    let mut last_ok = 0;
    if s.is_empty() {
        return (FullError(ABCParseError{
            kind : ABCErrorType::ExpectedTime,
            offset : (0,0)
        }),s);
    }
    for c in s.chars() {
        if let Some(n) = c.to_digit(10){
            let val_slide = val.and_then(|v|v.checked_mul(10));
            if let Some(new_val) = val_slide.and_then(|v|v.checked_add(n as i64)){
                val = Some(new_val);
                last_ok = new_val;
            }
            ns = &ns[1..];
        }else{
            if ns.len() == s.len(){
                return (PartialResult::FullError(ABCParseError{
                    kind : ABCErrorType::UnexpectedChar(c),
                    offset : (0,1)
                }),s);
            }else{
               break;
            }
        }
    }
    if let Some(val) = val{
        (Success(val),ns)
    }else{
        (PartialError { partial: last_ok, err: ABCParseError{
            kind : ABCErrorType::NumberOverflow,
            offset : (0,s.len()-ns.len())
        } },ns)
    }
}

pub fn parse_note(s: &str) -> (PartialResult<ANote, Vec<ABCParseError>>, &str) {
    let mut errors = Vec::new();
    let (h_res, ns1) = parse_note_name(s);
    let h_res = h_res.to_options();
    let (Some(height),may_h_error) = h_res else{
        return (FullError(vec![h_res.1.unwrap()]),s);
    };
    if let Some(err) = may_h_error {
        errors.push(err);
    }
    let (l_res, ns2) = parse_note_length(ns1);
    let duration;
    match l_res {
        Success(length) => {
            duration = length;
        }
        FullError(_err) => {
            duration = (1, 1);
        }
        PartialError { partial, mut err } => {
            err.add_offset(s.len() - ns1.len());
            errors.push(err);
            duration = partial;
        }
    }

    let (ns3,tie) = if ns2.starts_with('-'){
        (&ns2[1..],true)
    }else{
        (ns2,false)
    };

    if errors.is_empty() {
        (
            Success(ANote {
                height,
                duration,
                tie,
            }),
            ns3,
        )
    } else {
        (
            PartialError {
                partial: ANote {
                    height,
                    duration,
                    tie,
                },
                err: errors,
            },
            ns3,
        )
    }
}

pub fn parse_note_length(s: &str) -> (PartialResult<(u16, u16), ABCParseError>, &str) {
    #[derive(Clone, Copy)]
    enum State {
        Start,
        Numerator,
        Denominator,
        FirstSlash,
        NthSlash,
    }
    use ABCErrorType::*;
    use PartialResult::*;
    use State::*;
    let mut numerator: u16 = 1;
    let mut denominator: u16 = 1;
    let mut state = Start;
    let mut max_i: usize;
    let mut it = s.char_indices();
    loop {
        let (i, c1) = option_unzip(it.next());
        if let Some(i) = i {
            max_i = i;
        } else {
            max_i = s.len();
        }
        match (state, c1) {
            (Start, Some('/')) => {
                state = FirstSlash;
                denominator = 2;
            }
            (Start, Some(digit)) if digit.is_digit(10) => {
                state = Numerator;
                numerator = digit.to_digit(10).unwrap() as u16;
            }
            (Start, _) => {
                return (
                    FullError(ABCParseError {
                        kind: ExpectedTime,
                        offset: (0, 0),
                    }),
                    s,
                );
            }
            (Numerator, Some('/')) => {
                state = FirstSlash;
                denominator = 2;
            }
            (Numerator, Some(digit)) if digit.is_digit(10) => {
                numerator = numerator * 10 + digit.to_digit(10).unwrap() as u16;
            }
            (Numerator, Some(_)) => {
                break;
            }

            (FirstSlash, Some('/')) => {
                state = NthSlash;
                denominator *= 2;
            }
            (FirstSlash, Some(digit)) if digit.is_digit(10) => {
                state = Denominator;
                denominator = digit.to_digit(10).unwrap() as u16;
            }
            (FirstSlash, Some(_)) => {
                break;
            }

            (NthSlash, Some('/')) => {
                denominator *= 2;
            }
            (NthSlash, Some(_)) => {
                break;
            }
            (Denominator, Some(digit)) if digit.is_digit(10) => {
                denominator = denominator * 10 + digit.to_digit(10).unwrap() as u16;
            }
            (Denominator, Some(_)) => {
                break;
            }
            (NthSlash | FirstSlash | Denominator | Numerator, None) => {
                break;
            }
        }
    }
    if numerator == 0 || denominator == 0 {
        (
            PartialError {
                partial: (
                    if numerator == 0 { 1 } else { numerator },
                    if denominator == 0 { 1 } else { denominator },
                ),
                err: ABCParseError {
                    kind: InvalidTime,
                    offset: (0, max_i),
                },
            },
            &s[max_i..],
        )
    } else {
        (Success((numerator, denominator)), &s[max_i..])
    }
}

/// Consumes a single note in ABC notation.
/// Double sharps or flats are not supported.
/// Returns a tuple with the parsing result and the number of bytes consumed.
pub fn parse_note_name(s: &str) -> (PartialResult<NoteHeight, ABCParseError>, &str) {
    use ABCErrorType::*;
    use PartialResult::*;
    let Some(ch1) = s.chars().next() else{
        return (FullError(ABCParseError{kind : ExpectedNote, offset : (0,0)}),s);
    };
    let mut offset: usize = 0;
    let mut ns = s;
    let alt = match ch1 {
        '_' => {
            ns = &s[1..];
            offset += 1;
            -1
        }
        '^' => {
            ns = &s[1..];
            offset += 1;
            1
        }
        _ => 0,
    };
    let mut chars = ns.chars();
    let Some(ch2) = chars.next() else{
        return (FullError(ABCParseError{kind : ExpectedNote, offset : (0,1) }),s);
    };
    let Some(name) = char_note_name(ch2) else{
        return (FullError(ABCParseError{kind : UnexpectedChar(ch2), offset : (0,1)}),s);
    };
    offset += 1;
    let mut octave: i8 = if ch2.is_lowercase() { 1 } else { 0 };
    let mut failed_or_overflow = false;

    loop {
        let Some(next) = chars.next() else{
            break;
        };
        match next {
            '\'' => {
                if octave > 0 && octave < 7 {
                    octave += 1;
                } else {
                    failed_or_overflow = true;
                }
                offset += 1;
            }
            ',' => {
                if octave < 1 && octave > -7 {
                    octave -= 1;
                } else {
                    failed_or_overflow = true;
                }
                offset += 1;
            }

            _ => {
                break;
            }
        }
    }
    if failed_or_overflow {
        (
            PartialError {
                partial: NoteHeight { name, octave, alt },
                err: ABCParseError {
                    kind: ModifierOverflow,
                    offset: (0, offset),
                },
            },
            &s[offset..],
        )
    } else {
        (Success(NoteHeight { name, octave, alt }), &s[offset..])
    }
}

#[test]
pub fn test_parse_note() {
    use ABCErrorType::*;
    use NoteName::*;
    use PartialResult::*;
    assert_eq!(
        parse_note("bA"),
        (
            Success(ANote {
                height: NoteHeight {
                    name: B,
                    octave: 1,
                    alt: 0
                },
                duration: (1, 1),
                tie: false
            }),
            "A"
        )
    );

    assert_eq!(parse_note("D/--f"),
        (
            Success(ANote {
                height: NoteHeight {
                    name: D,
                    octave: 0,
                    alt: 0
                },
                duration: (1, 2),
                tie: true
            }),
            "-f"
        )
    );

    assert_eq!(
        parse_note("_g''//_B A"),
        (
            Success(ANote {
                height: NoteHeight {
                    name: G,
                    octave: 3,
                    alt: -1
                },
                duration: (1, 4),
                tie: false
            }),
            "_B A"
        )
    );

    assert_eq!(
        parse_note("^C,,9/16 A"),
        (
            Success(ANote {
                height: NoteHeight {
                    name: C,
                    octave: -2,
                    alt: 1
                },
                duration: (9, 16),
                tie: false
            }),
            " A"
        )
    );

    assert_eq!(
        parse_note(""),
        (
            FullError(vec![ABCParseError {
                kind: ExpectedNote,
                offset: (0, 0)
            }]),
            ""
        )
    );

    assert_eq!(
        parse_note("/5C"),
        (
            FullError(vec![ABCParseError {
                kind: UnexpectedChar('/'),
                offset: (0, 1)
            }]),
            "/5C"
        )
    );

    assert_eq!(
        parse_note("C,,,,,,,,0/0- "),
        (
            PartialError {
                partial: ANote {
                    height: NoteHeight {
                        name: C,
                        octave: -7,
                        alt: 0
                    },
                    duration: (1, 1),
                    tie: true
                },
                err: vec![
                    ABCParseError {
                        kind: ModifierOverflow,
                        offset: (0, 9)
                    },
                    ABCParseError {
                        kind: InvalidTime,
                        offset: (9, 12)
                    }
                ]
            },
            " "
        )
    );
}
#[test]
pub fn test_parse_note_name() {
    use ABCErrorType::*;
    use PartialResult::*;
    let (result, offset) = parse_note_name("A");
    assert_eq!(
        result,
        Success(NoteHeight {
            name: NoteName::A,
            octave: 0,
            alt: 0
        })
    );
    assert_eq!(offset, "");

    let (result, offset) = parse_note_name("c' other");
    assert_eq!(
        result,
        Success(NoteHeight {
            name: NoteName::C,
            octave: 2,
            alt: 0
        })
    );
    assert_eq!(offset, " other");

    let (result, offset) = parse_note_name("_d''B");
    assert_eq!(
        result,
        Success(NoteHeight {
            name: NoteName::D,
            octave: 3,
            alt: -1
        })
    );
    assert_eq!(offset, "B");

    let (result, offset) = parse_note_name("^B,,,9");
    assert_eq!(
        result,
        Success(NoteHeight {
            name: NoteName::B,
            octave: -3,
            alt: 1
        })
    );
    assert_eq!(offset, "9");

    let (result, offset) = parse_note_name("");
    assert_eq!(
        result,
        FullError(ABCParseError {
            kind: ExpectedNote,
            offset: (0, 0)
        })
    );
    assert_eq!(offset, "");

    let (result, offset) = parse_note_name("^^__");
    assert_eq!(
        result,
        FullError(ABCParseError {
            kind: UnexpectedChar('^'),
            offset: (0, 1)
        })
    );
    assert_eq!(offset, "^^__");
}

pub fn char_note_name(name: char) -> Option<NoteName> {
    use NoteName::*;
    match name {
        'C' | 'c' => Some(C),
        'D' | 'd' => Some(D),
        'E' | 'e' => Some(E),
        'F' | 'f' => Some(F),
        'G' | 'g' => Some(G),
        'A' | 'a' => Some(A),
        'B' | 'b' => Some(B),
        _ => None,
    }
}
#[test]
fn test_parse_rest(){
    use ABCErrorType::*;
    use ARestKind::*;
    assert_eq!(
        parse_rest("z"),
        (Success(ARest{
            visible : true,
            kind: RestRelative((1,1))
        }),"")
    );
    assert_eq!(
        parse_rest("Z "),
        (Success(ARest{
            visible : true,
            kind: RestMeasure(1)
        })," ")
    );
    assert_eq!(
        parse_rest("z3/2z"),
        (Success(ARest{
            visible : true,
            kind: RestRelative((3,2))
        }),"z")
    );
    assert_eq!(
        parse_rest("z5//0"),
        (Success(ARest{
            visible : true,
            kind: RestRelative((5,4))
        }),"0"));
    assert_eq!(
        parse_rest("Z2Z"),
        (Success(ARest{
            visible : true,
            kind: RestMeasure(2)
        }),"Z"));
    assert_eq!(
        parse_rest("RZ"),
        (FullError(ABCParseError{
            kind : ABCErrorType::ExpectedRest,
            offset : (0,1)
        }),"RZ"));
    
    assert_eq!(
        parse_rest("Z65700B"),
        (PartialError { partial:
            ARest{
                visible : true,
                kind : ARestKind::RestMeasure(u16::MAX)
            }, err: ABCParseError { kind: ABCErrorType::NumberOverflow, offset: (1,6) } },"B"));
    
}
#[test]
fn test_parse_time() {
    use ABCErrorType::*;
    use PartialResult::*;

    assert_eq!(
        parse_note_length(""),
        (
            FullError(ABCParseError {
                kind: ExpectedTime,
                offset: (0, 0)
            }),
            ""
        )
    );

    assert_eq!(parse_note_length("2"), (Success((2, 1)), ""));

    assert_eq!(parse_note_length("251"), (Success((251, 1)), ""));

    assert_eq!(parse_note_length("3 3"), (Success((3, 1)), " 3"));

    assert_eq!(parse_note_length("312 99 8"), (Success((312, 1)), " 99 8"));

    assert_eq!(parse_note_length("/"), (Success((1, 2)), ""));

    assert_eq!(parse_note_length("//"), (Success((1, 4)), ""));

    assert_eq!(parse_note_length("////BA"), (Success((1, 16)), "BA"));

    assert_eq!(parse_note_length("/ "), (Success((1, 2)), " "));

    assert_eq!(parse_note_length("/32BF"), (Success((1, 32)), "BF"));

    assert_eq!(parse_note_length("5/4/FIVE"), (Success((5, 4)), "/FIVE"));

    assert_eq!(parse_note_length("25/64/32"), (Success((25, 64)), "/32"));

    assert_eq!(parse_note_length("99///2/"), (Success((99, 8)), "2/"));
}
pub fn note_height_in_tonality(name: NoteName) {}

fn option_unzip<A, B>(r: Option<(A, B)>) -> (Option<A>, Option<B>) {
    if let Some((a, b)) = r {
        (Some(a), Some(b))
    } else {
        (None, None)
    }
}
