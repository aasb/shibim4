#[derive(Debug,PartialEq,Eq)]
pub enum PartialResult<T,E>{
    Success(T),
    PartialError{
        partial : T,
        err : E
    },
    FullError(E)
}
//Convenience trait 
trait OptionToPartialResult<T>{
    fn success_or<E>(self,err : E)->PartialResult<T,E>;
    fn partial_and<E>(self,err : E)->PartialResult<T,E>;
}

impl<T,E> PartialResult<T,E>{
    pub fn to_result_partial(self)-> (Result<T,E>,Option<T>){
        match self {
            Self::Success(t) => (Ok(t),None),
            Self::PartialError { partial, err } => (Err(err),Some(partial)),
            Self::FullError(err) => (Err(err),None)
        }
    }
    pub fn to_options(self) -> (Option<T>,Option<E>){
        match self {
            Self::Success(t) => (Some(t),None),
            Self::PartialError { partial, err } => (Some(partial),Some(err)),
            Self::FullError(err) => (None,Some(err))
        }
    }
    pub fn map<U,F>(self, f:F)->PartialResult<U,E>
    where F : FnOnce(T) -> U{
        match self {
            Self::Success(t) => PartialResult::Success(f(t)),
            Self::PartialError { partial, err } => PartialResult::PartialError { partial: f(partial), err },
            Self::FullError(err) => PartialResult::FullError(err)
        }
    }
    pub fn map_err<V,F>(self,f : F)->PartialResult<T,V>
    where F : FnOnce(E) -> V{
        match self {
            Self::Success(t) => PartialResult::Success(t),
            Self::PartialError { partial, err } => PartialResult::PartialError { partial, err: f(err) },
            Self::FullError(err) => PartialResult::FullError(f(err))
        }
    }
}

impl<T,E> From<Result<T,E>> for PartialResult<T,E>{
    fn from(r: Result<T,E>) -> Self {
        match r {
            Ok(t) => Self::Success(t),
            Err(e) => Self::FullError(e)
        }
    }
}

impl<T> OptionToPartialResult<T> for Option<T>{
    fn success_or<E>(self,err : E)->PartialResult<T,E> {
        match self{
            Some(u) => PartialResult::Success(u),
            None => PartialResult::FullError(err)
        }
    }
    fn partial_and<E>(self,err : E)->PartialResult<T,E> {
        match self{
            Some(partial) => PartialResult::PartialError { partial, err },
            None => PartialResult::FullError(err)
        }
    }
}