extern crate shibim_core as core;
use core::base::Song;
use core::files::CacheItem;
use core::files::PathsRef;
use core::files::ResourceType;
use core::i18n;
use std::fmt::Display;
use std::fs::File;
use std::io::Write;
use std::path::Path;
//Quick and dirty test app
fn main() {
    let dry_run = false;
    let mut paths =
        core::files::DefaultPathConf::new("dat/lst/", "dat/shb/", "shibim-site", "web/");
    paths.make_input_dirs().unwrap();
    paths.make_output_dirs().unwrap();
    paths.copy_static_assets().unwrap();
    paths.static_preload("js/song.js").unwrap();
    paths.static_preload("css/song.css").unwrap();
    paths.static_preload("css/song_page.css").unwrap();
    paths.static_preload("css/fonts.css").unwrap();

    //default_path.
    let shb_paths = core::files::get_dir_filelist_ext(paths.source(".", ResourceType::SHB), "shb")
        .map(|x| core::files::load_batch(x, core::files::read_shb, ""));
    if let Ok((mut cache, shb_errors)) = shb_paths {
        if !dry_run {
            std::fs::write(
                paths.output("search.html", ResourceType::STATIC),
                core::html::SearchPage {
                    lang: i18n::Lang::ES,
                    prov: &paths,
                }
                .to_string(), //TODO: Is this correct? seems inefficient to do concatenation
            )
            .unwrap();
        }
        write_json_text(
            paths.output("text.js", ResourceType::STATIC),
            &paths,
            cache.iter(),
        )
        .unwrap();
        for item in cache.iter() {
            if !dry_run {
                std::fs::write(
                    paths.output(item.handle.clone() + ".html", ResourceType::SHB),
                    core::html::SongPage {
                        song: &item.data,
                        prov: &paths,
                        static_js: false,
                        lang: core::i18n::Lang::ES,
                    }
                    .to_string(),
                )
                .unwrap();
            }
        }
        println!("{:?}", shb_errors);
        cache.sort_by(|a, b| b.data.name.cmp(&a.data.name));
        let index = cache
            .iter()
            .map(|song| core::files::song_cache_to_index(song, &paths))
            .collect();
        if !dry_run {
            std::fs::write(
                paths.output("songs.html", ResourceType::STATIC),
                core::html::SongIndex { list: index }.to_string(),
            )
            .unwrap();
        }

        let song_db = core::SongMemoryDatabase::new(cache);
        let lst_paths =
            core::files::get_dir_filelist_ext(paths.source(".", ResourceType::LST), "lst").unwrap();
        let (mut lst_cache, lst_errors) = core::process_lst_batch(lst_paths, "", &song_db,true);
        for item in &lst_cache {
            //println!("{:?} {:?}",item.file_name,item.modified);
            if !dry_run {
                std::fs::write(
                    paths.output(item.handle.clone() + ".html", ResourceType::LST),
                    core::html::SonglistPage {
                        list: &item.data,
                        prov: &paths,
                        static_js: false,
                        lang: core::i18n::Lang::ES,
                    }
                    .to_string(),
                )
                .unwrap();
            }
        }
        lst_cache.sort_by(|a, b| b.handle.cmp(&a.handle));
        let index = core::html::SonglistIndex {
            list: lst_cache
                .iter()
                .map(|lst| core::files::list_cache_to_index(lst, &paths))
                .collect(),
        };
        if !dry_run {
            std::fs::write(
                paths.output("songlists.html", ResourceType::STATIC),
                index.to_string(),
            )
            .unwrap();
        }
        println!("{:?}", lst_errors);
        
        //core::files::retrieve_cache(Path::new("cache.cbor")).unwrap();
        //core::files::save_cache(&uk.0,Path::new("cache.cbor"));
    } else {
        eprintln!("Could not open source dirs dat/lst, dat/shb");
    }

    //println!("{:?}",core::files::copy_dir("src", "src2"));
}

fn write_json_text<'i>(
    path: impl AsRef<Path>,
    hpaths: &impl PathsRef,
    cache_items: impl Iterator<Item = &'i CacheItem<Song>>,
) -> Result<(), std::io::Error> {
    let mut file = File::create(path)?;
    write!(file, "let _text_js = [")?;
    for (i, cache_song) in cache_items.enumerate() {
        if i > 0 {
            writeln!(file, ",").unwrap();
        }
        write_json_entry(
            &mut file,
            i,
            &cache_song.data.name,
            &cache_song.handle,
            hpaths.rel_out(
                cache_song.handle.clone() + ".html",
                ResourceType::SHB,
                ResourceType::STATIC,
            ),
            cache_song.data.norm_text(i18n::Lang::ES),
        );
    }
    write!(file, "]").unwrap();
    Ok(())
}

fn write_json_entry(
    file: &mut impl Write,
    id: usize,
    name: impl Display,
    handle: impl Display,
    href: impl Display,
    content: impl Display,
) {
    write!(
        file,
        "{{id:{}, name:\"{}\", handle:\"{}\", href:\"{}\", text:\"{}\"}}",
        id, name, handle, href, content
    )
    .expect("Error writing text extract");
}
