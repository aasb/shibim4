
pub trait Bigram{
    fn first(&self) -> &str;
    fn second(&self) -> &str;
}
impl Bigram for (String,String){
    fn first(&self) -> &str{
        self.0.as_ref()
    }
    fn second(&self) -> &str{
        self.1.as_ref()
    }
}

impl<'i> Bigram for (&'i str,&'i str){
    fn first(&self) -> &str{
        self.0
    }
    fn second(&self) -> &str{
        self.1
    }
}

impl<'a> std::borrow::Borrow<dyn Bigram + 'a> for (String, String)
{
    fn borrow(&self) -> &(dyn Bigram + 'a) {
        self
    }
}

impl std::hash::Hash for dyn Bigram +'_{
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.first().hash(state);
        self.second().hash(state);
    }
}

impl PartialEq for dyn Bigram + '_{
    fn eq(&self, other : &Self) -> bool{
        self.first() == other.first() &&
            self.second() == other.second()
    }
}

impl  Eq for dyn Bigram + '_ {}
