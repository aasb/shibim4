use std::collections::HashMap;
use std::hash::Hash;
use std::sync::Arc as SPtr;
use std::borrow::Borrow;

pub struct BiHashMap<K, V> {
    forward: HashMap<SPtr<K>, SPtr<V>>,
    reverse: HashMap<SPtr<V>, SPtr<K>>,
}

impl<K, V> BiHashMap<K, V>
where
    K: Hash + Eq,
    V: Hash + Eq,
{
    pub fn new()->Self{
        Self{
            forward : HashMap::new(),
            reverse : HashMap::new()
        }
    }
    pub fn insert(&mut self, one: K, two: V) {
        let one = SPtr::new(one);
        let two = SPtr::new(two);
        self.forward.insert(one.clone(), two.clone());
        self.reverse.insert(two, one);
    }
    pub fn clear(&mut self){
        self.forward.clear();
        self.reverse.clear();
    }
    pub fn get_forward(&self, key: impl Borrow<K>) -> Option<&V> {
        self.forward.get(key.borrow()).map(|m| m.as_ref())
    }
    pub fn get_reverse(&self, key: impl Borrow<V>) -> Option<&K> {
        self.reverse.get(key.borrow()).map(|m| m.as_ref())
    }
    
    pub fn remove_entry_forward(&mut self, key: impl Borrow<K>) -> Option<(K, V)> {
        let (key, value) = self.forward.remove_entry(key.borrow())?;
        self.reverse
            .remove_entry(&value)
            .expect("Bidirectional map constraint violated");
        Some((SPtr::try_unwrap(key).ok()?, SPtr::try_unwrap(value).ok()?))
    }
    pub fn remove_entry_reverse(&mut self, key: impl Borrow<V>) -> Option<(K, V)> {
        let (rev_key, rev_value) = self.reverse.remove_entry(key.borrow())?;
        self.forward
            .remove_entry(&rev_value)
            .expect("Bidirectional map constraint violated");
        Some((
            SPtr::try_unwrap(rev_value).ok()?,
            SPtr::try_unwrap(rev_key).ok()?,
        ))
    }

    pub fn iter(&self) -> impl Iterator<Item = (&K, &V)> {
        self.forward
            .iter()
            .map(|(rc1, rc2)| (rc1.as_ref(), rc2.as_ref()))
    }
}