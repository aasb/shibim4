//This code is abhorrent
//Inneficient and poorly designed data structure
//Tons of cloning
//I don't know what is where
//Doesn't clean up bigrams on deletion (Memory leak)
//Bad naming
extern crate qp_trie;
extern crate shibim_core;
use qp_trie::wrapper::BString;
use qp_trie::Trie;
use shibim_core::util::Itertools;
use shibim_core::*;
use std::{borrow::Cow, collections::HashMap};
extern crate rust_stemmers;
mod bigram;
mod bimap;
use bigram::Bigram;
use bimap::BiHashMap;
pub trait SongIndexer {
    fn index_song(&mut self, song: &base::Song, id: impl AsRef<str>);
    fn query_text(&self, text: impl AsRef<str>) -> Vec<&str>;
    fn remove_song(&mut self, id: impl AsRef<str>) -> bool;
    fn clear(&mut self);
}

fn lang_to_stemlang(lang: i18n::Lang) -> rust_stemmers::Algorithm {
    use i18n::Lang::*;
    use rust_stemmers::Algorithm::*;
    match lang {
        EN => English,
        ES => Spanish,
    }
}

struct DocData {
    w_total: usize,
    words: HashMap<String, usize>, //To be able to delete efficiently we need to know the words contained,
                                   //But sharing the ownership requires Arc, since this runs in a thread
                                   //But we would really like to use Rc, since there's no passing between threads
                                   //So instead we duplicate the string, again...
}
struct WordData {
    w_total: usize,
}
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct NodeData {
    text_count: usize,
    title_count: usize,
}
pub struct MemoryTrieIndexer {
    trie: Trie<BString, HashMap<usize, NodeData>>,
    docs: HashMap<usize, DocData>,
    lang: i18n::Lang,
    names: BiHashMap<String, usize>,
    bigrams: HashMap<(String, String), HashMap<usize, usize>>,
    words: HashMap<String, WordData>,
    total_words: usize,
    total_sqr_doc_words: f64,
    prefix_perf_match_mult: f64,
    title_mult: f64,
    index_counter: usize,
}

impl Default for MemoryTrieIndexer {
    fn default() -> Self {
        MemoryTrieIndexer {
            trie: Trie::new(),
            docs: HashMap::new(),
            words: HashMap::new(),
            bigrams: HashMap::new(),
            names: BiHashMap::new(),
            total_words: 0,
            total_sqr_doc_words: 0.0,
            prefix_perf_match_mult: 1.5,
            title_mult: 1.75,
            lang: i18n::Lang::EN,
            index_counter: 0,
        }
    }
}

impl MemoryTrieIndexer {
    pub fn new(lang: i18n::Lang) -> Self {
        MemoryTrieIndexer {
            trie: Trie::new(),
            docs: HashMap::new(),
            words: HashMap::new(),
            bigrams: HashMap::new(),
            names: BiHashMap::new(),
            total_words: 0,
            total_sqr_doc_words: 0.0,
            prefix_perf_match_mult: 1.5,
            title_mult: 1.75,
            lang,
            index_counter: 0,
        }
    }
    fn insert_title_word(&mut self, word: &str, doc_id: usize) {
        if let Some(set) = self.trie.get_mut_str(word) {
            if let Some(doc) = set.get_mut(&doc_id) {
                doc.title_count += 1;
            } else {
                set.insert(
                    doc_id,
                    NodeData {
                        text_count: 0,
                        title_count: 1,
                    },
                );
            }
        } else {
            self.trie.insert_str(
                word,
                HashMap::from([(
                    doc_id,
                    NodeData {
                        text_count: 0,
                        title_count: 1,
                    },
                )]),
            );
        }
    }
    fn insert_word(&mut self, word: &str, doc_id: usize) {
        if let Some(set) = self.trie.get_mut_str(word) {
            if let Some(doc) = set.get_mut(&doc_id) {
                doc.text_count += 1;
            } else {
                set.insert(
                    doc_id,
                    NodeData {
                        text_count: 1,
                        title_count: 0,
                    },
                );
            }
        } else {
            self.trie.insert_str(
                word,
                HashMap::from([(
                    doc_id,
                    NodeData {
                        text_count: 1,
                        title_count: 0,
                    },
                )]),
            );
        }

        if let Some(mut word_n) = self.words.get_mut(word) {
            word_n.w_total += 1;
        } else {
            self.words.insert(word.to_string(), WordData { w_total: 1 });
        }

        if let Some(doc) = self.docs.get_mut(&doc_id) {
            doc.w_total += 1;
            if let Some(count) = doc.words.get_mut(word) {
                *count += 1;
            } else {
                doc.words.insert(word.to_owned(), 1);
            }
        }
    }

    pub fn insert_bigram(&mut self, first: &str, second: &str, doc_id: usize) {
        let key = &(first, second) as &dyn Bigram;
        if let Some(hmap) = self.bigrams.get_mut(key) {
            hmap.entry(doc_id).and_modify(|c| *c += 1).or_insert(1);
        } else {
            self.bigrams.insert(
                (first.to_owned(), second.to_owned()),
                HashMap::from([(doc_id, 1)]),
            );
        }
    }
    /*
    fn w_exact_search(&self, s: &str) -> Vec<(usize, NodeData)> {
        if let Some(items) = self.trie.get_str(s) {
            items.iter().map(|(x, y)| (*x, *y)).collect()
        } else {
            Vec::new()
        }
    }
    fn w_prefix_search<'i>(
        &'i self,
        prefix: &'i str,
    ) -> impl Iterator<Item = TriePrefixSearchResult> + 'i {
        self.trie
            .iter_prefix_str(prefix)
            .flat_map(move |(matched, doc_count)| {
                doc_count
                    .iter()
                    .zip(std::iter::repeat(matched.as_str() == prefix))
            })
            .map(|((key, count), perfect)| TriePrefixSearchResult {
                doc_id: *key,
                freq: count.text_count,
                perfect,
            })
    }
    */
    fn w_prefix_search_unique<'i>(&'i self, prefix: &'i str) -> (HashMap<usize, f64>, f64) {
        let mut doc_unique: HashMap<usize, f64> = HashMap::new();
        let mut total_count: f64 = 0.0; //uh oh
        self.trie
            .iter_prefix_str(prefix)
            .flat_map(move |(matched, doc_count)| {
                doc_count
                    .iter()
                    .zip(std::iter::repeat(matched.as_str() == prefix))
            })
            .for_each(|((key, node), perfect)| {
                let text_count = node.text_count as f64;
                let title_count = node.title_count as f64;
                let count: f64 = if perfect {
                    (text_count + title_count * self.title_mult) * self.prefix_perf_match_mult
                } else {
                    text_count + title_count * self.title_mult
                };
                doc_unique
                    .entry(*key)
                    .and_modify(|old_count| {
                        *old_count += count;
                    })
                    .or_insert(count);
            });
        //Light squeeze
        for (_, count) in doc_unique.iter_mut() {
            *count = count.sqrt();
            total_count += *count;
        }
        (doc_unique, total_count)
    }
    /*
    pub fn w_sorted_prefix_search(&self, s: &str) -> Vec<(usize, f64)> {
        let idxs = self.w_prefix_search(s);
        let mut res_unique: HashMap<usize, (f64, f64)> = HashMap::new();
        idxs.into_iter().for_each(|result_entry| {
            let freq = if result_entry.perfect {
                let added_weight = (result_entry.freq as f64) * (self.prefix_perf_match_mult - 1.0);
                res_unique
                    .entry(result_entry.doc_id)
                    .and_modify(|v| v.1 += added_weight)
                    .or_insert((
                        0.0,
                        self.docs[&result_entry.doc_id].w_total as f64 + added_weight,
                    ));
                (result_entry.freq as f64) * self.prefix_perf_match_mult
            } else {
                result_entry.freq as f64
            };
            res_unique
                .entry(result_entry.doc_id)
                .and_modify(|v| {
                    v.0 += freq;
                })
                .or_insert((freq, self.docs[&result_entry.doc_id].w_total as f64));
        });
        let mut vec_result: Vec<_> = res_unique
            .into_iter()
            .map(|(doc, (freq, count))| (doc, (freq / count)))
            .collect();
        vec_result.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap()); //Nan?
        vec_result
    }*/

    pub fn sorted_prefix_search<'i>(
        &self,
        mut words: impl Iterator<Item = &'i str> + Clone,
    ) -> Vec<(usize, f64)> {
        //let mut docs:HashMap<usize,f64> = HashMap::new();
        let prefix_results_iter: Vec<_> = words
            .clone()
            .filter_map(|s: &str| {
                if s.len() > 2 {
                    Some(self.w_prefix_search_unique(s))
                } else {
                    None
                }
            })
            .collect();
        let mut n_search_words: usize = 0;
        let mut max_prefix_count: f64 = 0.0;
        for (_prefix_docs, prefix_count) in &prefix_results_iter {
            n_search_words += 1;
            if *prefix_count > max_prefix_count {
                max_prefix_count = *prefix_count;
            }
        }
        let mut prefix_results_unique: HashMap<usize, (f64, usize)> = HashMap::new();
        for (prefix_docs, all_prefix_count) in prefix_results_iter {
            for (doc_id, doc_prefix_count) in prefix_docs {
                prefix_results_unique
                    .entry(doc_id)
                    .and_modify(|(doc_value, words_in_doc)| {
                        *doc_value += doc_prefix_count.log2() - all_prefix_count.log2();
                        *words_in_doc += 1;
                    })
                    .or_insert((doc_prefix_count.log2() - all_prefix_count.log2(), 1));
            }
        }
        let max_penalty = max_prefix_count.log2();
        for (_doc_id, (doc_value, words_in_doc)) in prefix_results_unique.iter_mut() {
            *doc_value -= max_penalty * (n_search_words - *words_in_doc) as f64
        }

        let mut add_bigram = |word_a: &str, word_b: &str| {
            if let Some(docs) = self.bigrams.get(&(word_a, word_b) as &dyn Bigram) {
                for doc_id in docs.keys() {
                    prefix_results_unique
                        .entry(*doc_id)
                        .and_modify(|(doc_value, _n_words)| {
                            *doc_value += max_penalty * 0.5;
                        })
                        //We have choosen poorly
                        //.or_insert((-max_penalty * (n_search_words as f64 - 0.5), 0))
                        ;
                }
            }
        };

        let mut word_0 = None;
        for (word_1, word_2) in words.clone().zip({
            words.next();
            words
        }) {
            add_bigram(word_1, word_2);
            if let Some(word_0) = word_0 {
                add_bigram(word_0, word_2);
            }
            word_0 = Some(word_1);
        }

        let mut sorted: Vec<(usize, f64)> = prefix_results_unique
            .iter()
            .map(|(doc_id, (doc_val, _))| (*doc_id, *doc_val))
            .collect();
        sorted.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap());
        sorted
    }
}

impl SongIndexer for MemoryTrieIndexer {
    fn index_song(&mut self, song: &base::Song, id: impl AsRef<str>) {
        self.names
            .insert(id.as_ref().to_owned(), self.index_counter);
        let index = self.index_counter;
        self.docs.insert(
            index,
            DocData {
                w_total: 0,
                words: HashMap::new(),
            },
        );
        self.index_counter += 1;
        let stemmer = rust_stemmers::Stemmer::create(lang_to_stemlang(self.lang));
        let mut w_total = 0;
        for title_word in i18n::normalize_text(&song.name, self.lang).split_whitespace() {
            self.insert_title_word(title_word, index);
            //Also count total?
        }
        let words = song
            .all_text()
            .map(|x| i18n::normalize_text(x, self.lang))
            .join("");
        let mut prev_stem: Option<Cow<str>> = None;
        let mut prev2_stem: Option<Cow<str>> = None;
        for (word, stem) in words.split_whitespace().map(|r| (r, stemmer.stem(r))) {
            if let Some(ref prev_word) = prev_stem {
                self.insert_bigram(prev_word, &stem, index);
                if let Some(ref prev2_word) = prev2_stem {
                    self.insert_bigram(prev2_word, &stem, index);
                }
            }
            self.insert_word(word, index);
            w_total += 1;
            prev2_stem = prev_stem;
            prev_stem = Some(stem);
        }
        self.total_words += w_total;
        self.total_sqr_doc_words += (w_total as f64).sqrt();
    }
    fn query_text(&self, words: impl AsRef<str>) -> Vec<&str> {
        let norm_words : String = words.as_ref().chars().map(|c|c.to_lowercase()).flatten().collect();
        self.sorted_prefix_search(
                norm_words
                .split_whitespace(),
        )
        .into_iter()
        .map(|(doc_id, _)| self.names.get_reverse(doc_id).unwrap().as_ref())
        .collect()
    }
    fn clear(&mut self) {
        self.trie.clear();
        self.bigrams.clear();
        self.docs.clear();
        self.names.clear();
        self.total_words = 0;
        self.total_sqr_doc_words = 0.0;
        self.words.clear();
    }
    fn remove_song(&mut self, id: impl AsRef<str>) -> bool {
        //Another unncesary copy :c (Bimap is also highly innefficient)
        if let Some(idx) = self.names.get_forward(id.as_ref().to_owned()) {
            let docdata = self.docs.remove(idx).expect("Unsound index data structure");
            for (word, times) in docdata.words {
                let node = self
                    .trie
                    .get_mut_str(&word)
                    .expect("Unsound index data structure");
                node.remove(idx);
                self.words
                    .get_mut(&word)
                    .expect("Unsound index data structure")
                    .w_total -= times;
                //
            }
            //TODO: !mem_leak! remove bigram (as of now it would require traversing the whole map)
            true
        } else {
            false
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn basic_build_search() {
        let (song, _err) = parser::parse_shb_str(
            r##"name:taco
tonic : C
@E1 There was once
There once was a taco
that had had lettuce
A·`it did was too shallow
E·wise |C·was |F·`not`F· its sauce
@C1 this word
This word appears word
too many word times
please word stop this word
word 
"##,
        );
        let (song2, _err) = parser::parse_shb_str(
            r##"name:tonic
tonic : C
@E1 There was once
a wise men once wisely said wise 
---
on a moonlight song, no didn't
shouldn't could'aint
"##,
        );
        let mut index = MemoryTrieIndexer::default();
        index.index_song(&song, "Art");
        index.index_song(&song2, "Tart");
        //let node = index.trie.get_str("wise").unwrap();
        //assert_eq!(node.get(&0), Some(&1));
        //assert_eq!(node.get(&1), Some(&2));
        let node2 = index.trie.get_str("moonlight").unwrap();
        assert_eq!(node2.get(&0), None);
        //assert_eq!(node2.get(&1), Some(&1));
        //println!("{:?}", index.w_sorted_prefix_search("wise"));
        println!("{:?}", index.bigrams);

        let mut u = HashMap::new();
        u.insert(("rr".to_string(), "ww".to_string()), 1);
        println!("{:?}", u);
        println!("{:?}", u.get(&("rr", "ww") as &dyn Bigram));
    }
}
